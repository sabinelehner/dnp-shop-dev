<?php
/**
 * @author 		Vladimir Popov
 * @copyright  	Copyright (c) 2014 Vladimir Popov
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $this->getTable('webforms'),
        'crf_account',
        'tinyint(1) NOT NULL DEFAULT "0"'
    )
;
$installer->getConnection()
    ->addColumn(
        $this->getTable('webforms'),
        'crf_account_position',
        'int(11) NOT NULL DEFAULT "10"'
    )
;
$installer->getConnection()
    ->addColumn(
        $this->getTable('webforms'),
        'crf_account_frontend',
        'tinyint(1) NOT NULL DEFAULT "1"'
    )
;
$installer->getConnection()
    ->addColumn(
        $this->getTable('webforms'),
        'crf_account_group_serialized',
        'text NOT NULL'
    )
;

$installer->endSetup();