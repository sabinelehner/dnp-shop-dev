<?php
class ARW_Sebian_Model_System_Config_Source_Settings_Element_Footerstyle
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'style_1', 'label' => Mage::helper('adminhtml')->__('Footer Fashion Pro')),
            array('value' => 'style_2', 'label' => Mage::helper('adminhtml')->__('Footer Inter Pro')),
            array('value' => 'style_3', 'label' => Mage::helper('adminhtml')->__('Footer Cos Pro')),
            array('value' => 'style_4', 'label' => Mage::helper('adminhtml')->__('Footer Jewel Pro')),
            array('value' => 'style_5', 'label' => Mage::helper('adminhtml')->__('Footer Bake Pro')),
            array('value' => 'style_6', 'label' => Mage::helper('adminhtml')->__('Footer Bike Pro')),
            array('value' => 'style_7', 'label' => Mage::helper('adminhtml')->__('Footer Sun Pro')),
            array('value' => 'style_8', 'label' => Mage::helper('adminhtml')->__('Footer Bar Pro')),
            array('value' => 'style_9', 'label' => Mage::helper('adminhtml')->__('Footer Ele Pro')),
            array('value' => 'style_10', 'label' => Mage::helper('adminhtml')->__('Footer Ten Pro'))
        );
    }
}