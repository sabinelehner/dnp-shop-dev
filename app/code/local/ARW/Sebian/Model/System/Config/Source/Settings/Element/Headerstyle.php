<?php
class ARW_Sebian_Model_System_Config_Source_Settings_Element_Headerstyle
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'style_1', 'label' => Mage::helper('adminhtml')->__('Style Home 01')),
            array('value' => 'style_2', 'label' => Mage::helper('adminhtml')->__('Style Home 03')),
            array('value' => 'style_3', 'label' => Mage::helper('adminhtml')->__('Style Home 04')),
            array('value' => 'style_4', 'label' => Mage::helper('adminhtml')->__('Style Home 05')),
            array('value' => 'style_5', 'label' => Mage::helper('adminhtml')->__('Style Home 06')),
            array('value' => 'style_6', 'label' => Mage::helper('adminhtml')->__('Style Home 07')),
            array('value' => 'style_7', 'label' => Mage::helper('adminhtml')->__('Style Home 08')),
            array('value' => 'style_8', 'label' => Mage::helper('adminhtml')->__('Style Home 09')),
            array('value' => 'style_9', 'label' => Mage::helper('adminhtml')->__('Style Home 10')),
            array('value' => 'style_left', 'label' => Mage::helper('adminhtml')->__('Style Header Left'))
        );
    }
}