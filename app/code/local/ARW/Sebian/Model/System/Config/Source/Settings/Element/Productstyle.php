<?php
class ARW_Sebian_Model_System_Config_Source_Settings_Element_Productstyle
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'option_style_1', 'label' => Mage::helper('adminhtml')->__('Style 1')),
            array('value' => 'option_style_2', 'label' => Mage::helper('adminhtml')->__('Style 2')),
            array('value' => 'option_style_3', 'label' => Mage::helper('adminhtml')->__('Style 3')),
            array('value' => 'option_style_4', 'label' => Mage::helper('adminhtml')->__('Style 4'))
        );
    }
}