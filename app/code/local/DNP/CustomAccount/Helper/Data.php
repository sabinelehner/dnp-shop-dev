<?php
class DNP_CustomAccount_Helper_Data extends Mage_Customer_Helper_Data {

    public function getLoginUrl()
    {		
		if (Mage::app()->getLocale()->getLocaleCode() == 'de_DE') {
			$storeID = 1;
		} else {
			$storeID = 2;
		}
		
		$loginUrl = Mage::getUrl('customer/account/login', array(
			'_store'=>$storeID
		));
		
		/*
		Mage::log('---', null, 'dnp-log.log');
		Mage::log('Default - URL: ' . $this->_getUrl(self::ROUTE_ACCOUNT_LOGIN, $this->getLoginUrlParams()), null, 'dnp-log.log');
		Mage::log('Current Locale: ' . Mage::app()->getLocale()->getLocaleCode(), null, 'dnp-log.log');
		Mage::log('DNP_CustomAccount_Helper_Data - URL: ' . $loginUrl, null, 'dnp-log.log');
		*/
		
		return $loginUrl;
		
        #return $this->_getUrl(self::ROUTE_ACCOUNT_LOGIN, $this->getLoginUrlParams());
    }
    
}
?>