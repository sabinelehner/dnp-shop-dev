<?php
/**
 * Overrite Customer model
 */

class DNP_CustomAccount_Model_Customer extends Mage_Customer_Model_Customer {
	
    public function sendPasswordInitConfirmationEmail()
    {
        $storeId = Mage::app()->getStore()->getId();
        if (!$storeId) {
            $storeId = $this->_getWebsiteStoreId();
        }
			
		/*
        $this->_sendEmailTemplate('customer/password/forgot_email_template', 'customer/password/forgot_email_identity',
            array('customer' => $this), $storeId);
		*/
		/*
        $this->_sendInitEmailTemplate('customer/password/forgot_email_identity',
            array('customer' => $this), $storeId);
		*/
        $this->_sendInitEmailTemplate('customer/password/forgot_email_identity',
            array('customer' => $this), $storeId);

        return $this;
    }
	
    protected function _sendInitEmailTemplate($sender, $templateParams = array(), $storeId = null, $customerEmail = null)
    {
        $customerEmail = ($customerEmail) ? $customerEmail : $this->getEmail();
        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($customerEmail, $this->getName());
        $mailer->addEmailInfo($emailInfo);

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig($sender, $storeId));
        $mailer->setStoreId($storeId);
        #$mailer->setTemplateId(Mage::getStoreConfig($template, $storeId));
        $mailer->setTemplateId('customer_init_password_email_template');
        $mailer->setTemplateParams($templateParams);
        $mailer->send();
        return $this;
    }
}