<?php
class DNP_CustomAccount_Model_Observer {
	
    public function checkB2B(Varien_Event_Observer $observer) {
		
		/*
		Mage::log('---', null, 'dnp-log.log');
		Mage::log('checkB2B!!!', null, 'dnp-log.log');
		Mage::log('mage store: ' . Mage::app()->getStore()->getStoreId(), null, 'dnp-log.log');
		*/
		
		if ($customer = Mage::getSingleton('customer/session')->getCustomer()) {
			
			/*
			Mage::log('customer is logged in: ' . $customer->getId(), null, 'dnp-log.log');
			Mage::log('customer website: ' . $customer->getWebsiteId(), null, 'dnp-log.log');
			Mage::log('mage website: ' . Mage::app()->getWebsite()->getId(), null, 'dnp-log.log');
			*/
			
			$custProductlangIDs = array();
			
			if ($customer->getProductlang())
				$custProductlangIDs = explode(',', $customer->getProductlang());
				
			if (is_array($custProductlangIDs) && (count($custProductlangIDs) > 0) && !in_array(Mage::app()->getStore()->getStoreId(), $custProductlangIDs)) {
				
				Mage::app()->setCurrentStore($custProductlangIDs[0]);
		
			} elseif (Mage::app()->getWebsite()->getId() != $customer->getWebsiteId()) {
			
				$defaultStoreId = Mage::app()
					->getWebsite($customer->getWebsiteId())
					->getDefaultGroup()
					->getDefaultStoreId();
					
				Mage::app()->setCurrentStore($defaultStoreId);	
			}
			
		}

    }
	
	 public function addLink(Varien_Event_Observer $observer) {

        $helper = Mage::helper('customer');

        if (!$helper->isLoggedIn()) {
            return;
        }

        $customer = $helper->getCustomer();

        if ($customer->getId() != 6366) {
            return;
        }

        $update = $observer->getLayout()->getUpdate();

        if (in_array('customer_account', $update->getHandles())) {
            $update->addUpdate('
                <reference name="customer_account_navigation">
					<action method="addLink" translate="label" ifconfig="affiliate/config/enabled"><name>affiliate</name><path>affiliate/index/index/</path><label>My Affiliate Account</label></action>
                </reference>
            ');
        }
    }
	
}
