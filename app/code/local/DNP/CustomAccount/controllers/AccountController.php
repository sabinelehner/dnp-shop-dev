<?php

/**
 * Customer account controller
 */
require_once 'Mage/Customer/controllers/AccountController.php';

class DNP_CustomAccount_AccountController extends Mage_Customer_AccountController {
	
    public function dnpGenTokensAction() {	
		
		$adminEmail=Mage::getSingleton('customer/session')->getCustomer()->getEmail();
		$curEmail = 'sabine.lehner@test.net';
		
        if (strcmp($adminEmail, 'sabine.lehner@gmx.net') != 0) {
			echo 'DNP :-(';
		} else {
			/*
			$allCustomers = Mage::getResourceModel('customer/customer_collection')
				#->addAttributeToSelect('email')
				#->addAttributeToFilter('is_active', 1)
				;
				*/
			
			$allCustomers = Mage::getResourceModel('customer/customer_collection');
			#$allCustomers->addAttributeToFilter('refcode', array('eq' => 1));
					
			foreach ($allCustomers as $customer) {
				#echo $item->getEmail() . "<br />";
			   
				$customer = Mage::getModel('customer/customer')
					->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
					->loadByEmail($customer->getEmail());
				if ($customer->getId()) {
					try {
						$newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
						$customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
						#$customer->sendPasswordResetConfirmationEmail();
					} catch (Exception $exception) {
						Mage::log($exception);
					}
				}
				
				echo 'email: ' . $customer->getEmail() . '<br />';
				echo 'init-url: http://devshop.deepnatureproject.com/de/customer/account/initpassword/?id='.$customer->getId().'&token=' . $newResetPasswordLinkToken . '<br />';
				echo 'token: ' . $newResetPasswordLinkToken . '<br /><br />';
			}
			
			echo 'DNP :-)';
		}
    }
	
    public function preDispatch()
    {
        // a brute-force protection here would be nice

        #parent::preDispatch();
		Mage_Core_Controller_Front_Action::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = strtolower($this->getRequest()->getActionName());
		
        $openActions = array(
            'create',
            'login',
            'logoutsuccess',
            'forgotpassword',
            'forgotpasswordpost',
            'changeforgotten',
            'changeinit',
            'resetpassword',
            'resetpasswordpost',
            'confirm',
            'confirmation',
            'initpassword',
            'dnpgentokens'
        );
        $pattern = '/^(' . implode('|', $openActions) . ')/i';

        if (!preg_match($pattern, $action)) {
            if (!$this->_getSession()->authenticate($this)) {
                $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }
	
    public function initPasswordAction_OLD()
    {
        try {
            $customerId = (int)$this->getRequest()->getQuery("id");
            $resetPasswordLinkToken = (string)$this->getRequest()->getQuery('token');
			
			#echo "id: " . $customerId . "<br>";
			#echo $resetPasswordLinkToken;
			#exit();

            #$this->_validateResetPasswordLinkToken($customerId, $resetPasswordLinkToken);
            $this->_validateInitPasswordLinkToken($customerId, $resetPasswordLinkToken);
			
            $this->_saveInitPasswordParameters($customerId, $resetPasswordLinkToken)
                ->_redirect('*/*/changeinit');

        } catch (Exception $exception) {
            $this->_getSession()->addError($this->_getHelper('customer')->__('Your password reset link is not valid.'));
            #$this->_redirect('*/*/forgotpassword');
        }
    }
	
    public function initPasswordAction()
    {
        $this->loadLayout();

        $this->getLayout()->getBlock('initPassword')->setEmailValue(
            $this->_getSession()->getForgottenEmail()
        );
        $this->_getSession()->unsForgottenEmail();

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    /**
     * Forgot customer password action
     */
    public function initPasswordPostAction()
    {
        $email = (string) $this->getRequest()->getPost('email');
        if ($email) {
            /**
             * @var $flowPassword Mage_Customer_Model_Flowpassword
             */
            $flowPassword = $this->_getModel('customer/flowpassword');
            $flowPassword->setEmail($email)->save();

            if (!$flowPassword->checkCustomerForgotPasswordFlowEmail($email)) {
                $this->_getSession()
                    ->addError($this->__('You have exceeded requests to times per 24 hours from 1 e-mail.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            if (!$flowPassword->checkCustomerForgotPasswordFlowIp()) {
                $this->_getSession()->addError($this->__('You have exceeded requests to times per hour from 1 IP.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $this->_getSession()->setForgottenEmail($email);
                $this->_getSession()->addError($this->__('Invalid email address.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }
            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);
			
			/*
            Mage::log('---', null, 'dnp-log.log');
            Mage::log('flowPassword: ' . $flowPassword, null, 'dnp-log.log');
            Mage::log('customer-email: ' . $email, null, 'dnp-log.log');
            Mage::log('customer-id: ' . $customer->getId(), null, 'dnp-log.log');
            Mage::log('customer-website: ' . $customer->getWebsiteId(), null, 'dnp-log.log');
            Mage::log('website: ' . Mage::app()->getStore()->getWebsiteId(), null, 'dnp-log.log');
			*/

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken =  $this->_getHelper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    //$customer->sendPasswordResetConfirmationEmail();
					
					$customer->setConfirmation(null);
					$customer->save();
					
                    $customer->sendPasswordInitConfirmationEmail();
                } catch (Exception $exception) {
                    $this->_getSession()->addError($exception->getMessage());
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            $this->_getSession()
                ->addSuccess( $this->_getHelper('customer')
                ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                    $this->_getHelper('customer')->escapeHtml($email)));
            $this->_redirect('*/*/');
            return;
        } else {
            $this->_getSession()->addError($this->__('Please enter your email.'));
            $this->_redirect('*/*/forgotpassword');
            return;
        }
    }
	
    protected function _saveInitPasswordParameters($customerId, $resetPasswordLinkToken)
    {
		/*
		echo self::CUSTOMER_ID_SESSION_NAME . "<br />";
		echo self::TOKEN_SESSION_NAME . "<br />";
		
		echo "---<br />";
		
		echo $customerId . "<br />";
		echo $resetPasswordLinkToken . "<br />";
		*/
		
        $this->_getSession()
            ->setData(self::CUSTOMER_ID_SESSION_NAME, $customerId)
            ->setData(self::TOKEN_SESSION_NAME, $resetPasswordLinkToken);

        return $this;
    }
	
    protected function _validateInitPasswordLinkToken($customerId, $resetPasswordLinkToken)
    {
        if (!is_int($customerId)
            || !is_string($resetPasswordLinkToken)
            || empty($resetPasswordLinkToken)
            || empty($customerId)
            || $customerId < 0
        ) {
            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Invalid password reset token.'));
        }

        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $this->_getModel('customer/customer')->load($customerId);
        if (!$customer || !$customer->getId()) {
            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Wrong customer account specified.'));
        }

        $customerToken = $customer->getRpToken();
        if (strcmp($customerToken, $resetPasswordLinkToken) != 0) {
            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Your password reset link has expired.'));
        }
    }
	
    public function changeInitAction()
    {
        try {
            list($customerId, $resetPasswordLinkToken) = $this->_getRestorePasswordParameters($this->_getSession());
            $this->_validateResetPasswordLinkToken($customerId, $resetPasswordLinkToken);
			#$this->_validateInitPasswordLinkToken($customerId, $resetPasswordLinkToken);
			
            $this->loadLayout();
            $this->renderLayout();

        } catch (Exception $exception) {
            $this->_getSession()->addError($this->_getHelper('customer')->__('Your password reset link has expired.'));
            $this->_redirect('*/*/forgotpassword');
        }
    }	
	
	/*
    public function loginAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('');
            return;
        }
        $this->getResponse()->setHeader('Login-Required', 'true');
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
		
		Mage::app()->setCurrentStore(10);
    }
	
    public function indexAction()
    {
		Mage::log('curWebsite: ' . Mage::app()->getWebsite()->getId(), null, 'dnp-log.log');
		Mage::log('curCustomer: ' . Mage::getSingleton('customer/session')->getCustomer()->getId(), null, 'dnp-log.log');
		parent::indexAction();
    }
	*/
	
    public function loginAction()
    {
		#Mage::log('loginAction!!! ' . Mage::app()->getWebsite()->getId(), null, 'dnp-log.log');
		
		$lastUrl = Mage::getSingleton('core/session')->getLastUrl();
		
		if ((strpos($lastUrl, '?dl=1') !== false) || (strpos($lastUrl, 'utm_source') !== false)) {
			#Mage::log('lastUrl: ' . $lastUrl, null, 'dnp-log.log');
			Mage::getSingleton('core/session')->setDNPLastUrl($lastUrl);
		}
		
		if (Mage::app()->getLocale()->getLocaleCode() == 'de_DE') {
			Mage::app()->setCurrentStore(1);
			#Mage::log('loginAction - setcurrentstore 1', null, 'dnp-log.log');
		} else {
			Mage::app()->setCurrentStore(2);
			#Mage::log('loginAction - setcurrentstore 2', null, 'dnp-log.log');
		}
		
		parent::loginAction();
    }
	
    public function loginPostAction()
    {
		#$curWebsite = Mage::app()->getWebsite()->getId();
							
        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*/');
            return;
        }

        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session = $this->_getSession();

        if ($this->getRequest()->isPost()) {	
		
			$login = $this->getRequest()->getPost('login');
			
			if (array_key_exists('setnew', $this->getRequest()->getParams())) {
				if (!empty($login['username'])) {
					
					$customer = Mage::getModel("customer/customer"); 
					$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
					$customer->loadByEmail($login['username']);
					
					if ($customer->getId()) {
						try {
							$newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
							$customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
							$customer->sendPasswordInitConfirmationEmail();
							$session->addSuccess($this->__('Email has been sent successfully.'));
						} catch (Exception $exception) {
							Mage::log($exception);
						}
					} else {
						$session->addError($this->__('Email not exists.'));
					}
				} else {
					$session->addError($this->__('Login is required.'));
				}
			} else {
				
				if (!empty($login['username']) && !empty($login['password'])) {
					
					/*
					$customer = Mage::getModel("customer/customer"); 
					
					$allWebsites = Mage::app()->getWebsites(); //get list of all stores,websites
					foreach ($allWebsites as $id => $website) {
						$customer->setWebsiteId($id);
						$customer->loadByEmail($login['username']);
						if ($customer->getId() && ($id != $curWebsite)) {
							
							$defaultStoreId = Mage::app()
								->getWebsite($id)
								->getDefaultGroup()
								->getDefaultStoreId();
									
							Mage::app()->setCurrentStore($defaultStoreId);
							Mage::getSingleton('core/session')->setData('login-form',new Varien_Object($login));
							$session->setAfterAuthUrl(Mage::getUrl('customer/account/login', array('_nosid' => true)));
						}
					}
					*/
					
					try {
						$session->login($login['username'], $login['password']);
						if ($session->getCustomer()->getIsJustConfirmed()) {
							$this->_welcomeCustomer($session->getCustomer(), true);
						}
					} catch (Mage_Core_Exception $e) {
						
						#Mage::getSingleton('core/session')->unsetData('login-form');
						
						switch ($e->getCode()) {
							case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
								$value = $this->_getHelper('customer')->getEmailConfirmationUrl($login['username']);
								$message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
								break;
							case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
								$message = $e->getMessage();
								break;
							default:
								$message = $e->getMessage();
						}
						$session->addError($message);
						$session->setUsername($login['username']);
					} catch (Exception $e) {
						// Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
					}
				} else {
					$session->addError($this->__('Login and password are required.'));
				}
			}
        }

        $this->_loginPostRedirect();
    }
	
    protected function _loginPostRedirect()
    {
		$session = $this->_getSession();
			
		/*
		Mage::log('getBeforeAuthUrl: ' . $session->getBeforeAuthUrl(), null, 'dnp-log.log');
		Mage::log('getAfterAuthUrl: ' . $session->getAfterAuthUrl(), null, 'dnp-log.log');
		
		$lastUrl = Mage::getSingleton('core/session')->getLastUrl();
		*/
			
		$customer = Mage::getSingleton('customer/session')->getCustomer();
		
		if (Mage::app()->getWebsite()->getId() != $customer->getWebsiteId()) {
			
			if ($customer->getProductlang())
				$custProductlangIDs = explode(',', $customer->getProductlang());
			else
				$custProductlangIDs = 0;
				
			if (is_array($custProductlangIDs) && (count($custProductlangIDs) > 0)) {
				$session->setBeforeAuthUrl(Mage::getUrl('customer/account', array(
						'_store'=>$custProductlangIDs[0]
					))
				);
				
				$defaultStoreCode = Mage::getModel('core/store')->load($custProductlangIDs[0])->getCode();
				
			} else {
			
				$defaultStoreId = Mage::app()
					->getWebsite($customer->getWebsiteId())
					->getDefaultGroup()
					->getDefaultStoreId();
					
				$session->setBeforeAuthUrl(Mage::getUrl('customer/account', array(
						'_store'=>$defaultStoreId
					))
				);
				
				$defaultStoreCode = Mage::getModel('core/store')->load($defaultStoreId)->getCode();
				
			}
				
			if (Mage::getSingleton('core/session')->getDNPLastUrl() && (strpos(Mage::getSingleton('core/session')->getDNPLastUrl(), '_en/') !== false)) {
				$urlParts = explode('/', Mage::getSingleton('core/session')->getDNPLastUrl());
				$defaultStoreCode = $urlParts[3];
			}
		
			if (Mage::getSingleton('core/session')->getDNPLastUrl() && (strpos(Mage::getSingleton('core/session')->getDNPLastUrl(), '/' . $defaultStoreCode . '/') !== false)) {
				$session->setBeforeAuthUrl(Mage::getSingleton('core/session')->getDNPLastUrl());
				Mage::getSingleton('core/session')->unsDNPLastUrl();
			}
		}
		
		parent::_loginPostRedirect();
    }
	
    public function logoutAction()
    {
		#Mage::getSingleton('core/session')->unsetData('login-form');
		#Mage::app()->setCurrentStore(1);	
							   
		if (Mage::app()->getLocale()->getLocaleCode() == 'de_DE') {
			Mage::app()->setCurrentStore(1);
			#Mage::log('setcurrentstore 1', null, 'dnp-log.log');
		} else {
			Mage::app()->setCurrentStore(2);
			#Mage::log('setcurrentstore 2', null, 'dnp-log.log');
		}
		
		parent::logoutAction();
    }
	
    public function createAction()
    {
		#Mage::app()->setCurrentStore(1);	
		if (Mage::app()->getLocale()->getLocaleCode() == 'de_DE') {
			Mage::app()->setCurrentStore(1);
		} else {
			Mage::app()->setCurrentStore(2);
		}
		
		parent::createAction();
    }
	
	/*	
    public function createAction()
    {
		Mage::getSingleton('core/session')->unsetData('login-form');
		parent::createAction();
    }
	
    public function createAction()
    {
		# Disable default registration for b2b
		if (Mage::app()->getStore()->getWebsiteId() != 1)
			$this->_redirect('');
		else
			parent::createAction();
    }
	*/
	
    public function createPostAction()
    {
        $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));

        if (!$this->_validateFormKey()) {
            $this->_redirectError($errUrl);
            return;
        }

        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->_redirectError($errUrl);
            return;
        }

        $customer = $this->_getCustomer();
		
		/* Register Customer to right Website */
		if ($this->getRequest()->getPost('webform_id', false) == 4) {
			/* Gro�h�ndler */
			$customer->setWebsiteId(4);
		} elseif ($this->getRequest()->getPost('webform_id', false) == 3) {
			/* Wiederverk�ufer */
			$customer->setWebsiteId(5);
		}
			
		if(!$this->checkWawiEmails($this->getRequest()->getParam('email', false))){
			$errors[] = $this->__('Customer email already exists in WaWi');
			$this->_addSessionError($errors);
		}else{
			try {
				$errors = $this->_getCustomerErrors($customer);
	
				if (empty($errors)) {
					$customer->cleanPasswordsValidationData();
					$customer->save();
					$this->_dispatchRegisterSuccess($customer);
					$this->_successProcessRegistration($customer);
					return;
				} else {
					$this->_addSessionError($errors);
				}
			} catch (Mage_Core_Exception $e) {
				$session->setCustomerFormData($this->getRequest()->getPost());
				if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
					$url = $this->_getUrl('customer/account/forgotpassword');
					$message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
				} else {
					$message = $this->_escapeHtml($e->getMessage());
				}
				$session->addError($message);
			} catch (Exception $e) {
				$session->setCustomerFormData($this->getRequest()->getPost());
				$session->addException($e, $this->__('Cannot save the customer.'));
			}
			
		}

        $this->_redirectError($errUrl);
    }
	
    protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
    {
        $this->_getSession()->addSuccess(
            $this->__('Thank you for registering with %s.', Mage::app()->getStore()->getFrontendName())
        );
        if ($this->_isVatValidationEnabled()) {
            // Show corresponding VAT message to customer
            $configAddressType =  $this->_getHelper('customer/address')->getTaxCalculationAddressType();
            $userPrompt = '';
            switch ($configAddressType) {
                case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation',
                        $this->_getUrl('customer/address/edit'));
                    break;
                default:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation',
                        $this->_getUrl('customer/address/edit'));
            }
            $this->_getSession()->addSuccess($userPrompt);
        }

        $customer->sendNewAccountEmail(
            $isJustConfirmed ? 'confirmed' : 'registered',
            '',
            Mage::app()->getStore()->getId(),
            $this->getRequest()->getPost('password')
        );

        $successUrl = $this->_getUrl('*/*/index', array('_secure' => true));		
		
        if ($this->_getSession()->getBeforeAuthUrl()) {
            $successUrl = $this->_getSession()->getBeforeAuthUrl(true);
        }
			
		$defaultStoreId = Mage::app()
			->getWebsite($customer->getWebsiteId())
			->getDefaultGroup()
			->getDefaultStoreId();
		
		$successUrl = Mage::getUrl('customer/account', array(
			'_store'=>$defaultStoreId
		));
		
		#Mage::log('---', null, 'dnp-log.log');
		#Mage::log('successUrl (DNP - CustomAccountController: ' . $successUrl, null, 'dnp-log.log');
		
        return $successUrl;
    }
	
    public function forgotPasswordPostAction()
    {
        $email = (string) $this->getRequest()->getPost('email');
        if ($email) {
            /**
             * @var $flowPassword Mage_Customer_Model_Flowpassword
             */
            $flowPassword = $this->_getModel('customer/flowpassword');
            $flowPassword->setEmail($email)->save();

            if (!$flowPassword->checkCustomerForgotPasswordFlowEmail($email)) {
                $this->_getSession()
                    ->addError($this->__('You have exceeded requests to times per 24 hours from 1 e-mail.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            if (!$flowPassword->checkCustomerForgotPasswordFlowIp()) {
                $this->_getSession()->addError($this->__('You have exceeded requests to times per hour from 1 IP.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $this->_getSession()->setForgottenEmail($email);
                $this->_getSession()->addError($this->__('Invalid email address.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken =  $this->_getHelper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                } catch (Exception $exception) {
                    $this->_getSession()->addError($exception->getMessage());
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            $this->_getSession()
                ->addSuccess( $this->_getHelper('customer')
                ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                    $this->_getHelper('customer')->escapeHtml($email)));
            $this->_redirect('*/*/forgotpassword');
            return;
        } else {
            $this->_getSession()->addError($this->__('Please enter your email.'));
            $this->_redirect('*/*/forgotpassword');
            return;
        }
    }
	
	/*
    protected function _successProcessRegistration(Mage_Customer_Model_Customer $customer)
    {
        $session = $this->_getSession();
			
		$defaultStoreId = Mage::app()
			->getWebsite($customer->getWebsiteId())
			->getDefaultGroup()
			->getDefaultStoreId();
		
		$session->setBeforeAuthUrl(Mage::getUrl('customer/account', array(
				'_store'=>$defaultStoreId
			))
		);
		
        if ($customer->isConfirmationRequired()) {
            $app = $this->_getApp();
            $store = $app->getStore();
            $customer->sendNewAccountEmail(
                'confirmation',
                $session->getBeforeAuthUrl(),
                $store->getId(),
                $this->getRequest()->getPost('password')
            );
            $customerHelper = $this->_getHelper('customer');
            $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.',
                $customerHelper->getEmailConfirmationUrl($customer->getEmail())));
			
			# _getUrl �NDERN!!!
            $url = $this->_getUrl('index', array('_secure' => true));
        } else {
            $session->setCustomerAsLoggedIn($customer);
            $url = $this->_welcomeCustomer($customer);
        }
        $this->_redirectSuccess($url);
        return $this;
    }
	*/
	
    public function checkWawiEmails($email)
    {
		return true;
		
		/*
		DEV WaWi
		https://devwawi.deepnatureproject.com/www/index.php?module=api&action=ExportVorlageGet&hash=f7777f3f21c8b05e12f6bc10ae12d0bf8ec9687a&id=4
		*/
		#$wawiurl = 'http://devwawi.deepnatureproject.com';
		#$hash = 'f7777f3f21c8b05e12f6bc10ae12d0bf8ec9687a';
		#$hash = '181f126c2b3b43779f1f6c54c6fea4eb8149fdbd';
		$wawiurl = 'https://wawi.deepnatureproject.com';
		$hash = 'cb8a3fbd652d7fa865f1da5ebc357d5596e8a563';
		
		$domain = 'https://deepnatureproject.com';
		$key = 'EXsl4ywgDR5mR3sTGwws9Ikl2ocSKZlvhxoFTv8Au3MaV5UqrEIJORVF5PmFNy';
		$date = gmdate('dmY');
		$hash = "";		
		for ($i = 0; $i <= 200; $i++) {
			$hash = sha1($hash . $key . $domain . $date);
		}
		
		$url = $wawiurl.'/www/index.php?module=api&action=ExportVorlageGet&&hash='.$hash.'&id=7';
		
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST'
			),
		);
		$context = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		$result = str_replace('&', '&amp;', $result);
		
		$xml = simplexml_load_string($result);
		
		/*
		var_dump($result);
		exit();
		*/
		
		if ($xml->status->messageCode != '1') {
			return false;
		} else {
			$pos = strpos($result, $email);
		
			if ($pos === false) {
				return true;
			} else {
				return false;
			}
		}
    }
	
	/*
    public function getCouponAction()
    {
        $this->loadLayout();
		
		// Get the rule in question
		$rule = Mage::getModel('salesrule/rule')->load(12); //21 = ID of coupon in question
		
		// Define a coupon code generator model instance
		// Look at Mage_SalesRule_Model_Coupon_Massgenerator for options
		$generator = Mage::getModel('salesrule/coupon_massgenerator');
		
		$parameters = array(
			#'count'=>5,
			'format'=>'alphanumeric',
			'dash_every_x_characters'=>4,
			'prefix'=>'DNP-',
			'suffix'=>'-VQU',
			'length'=>8
		);
		
		if( !empty($parameters['format']) ){
		  switch( strtolower($parameters['format']) ){
			case 'alphanumeric':
			case 'alphanum':
			  $generator->setFormat( Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHANUMERIC );
			  break;
			case 'alphabetical':
			case 'alpha':
			  $generator->setFormat( Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHABETICAL );
			  break;
			case 'numeric':
			case 'num':
			  $generator->setFormat( Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_NUMERIC );
			  break;
		  }
		}
		
		$generator->setDash( !empty($parameters['dash_every_x_characters'])? (int) $parameters['dash_every_x_characters'] : 0);
		$generator->setLength( !empty($parameters['length'])? (int) $parameters['length'] : 6);
		$generator->setPrefix( !empty($parameters['prefix'])? $parameters['prefix'] : '');
		$generator->setSuffix( !empty($parameters['suffix'])? $parameters['suffix'] : '');
		
		// Set the generator, and coupon type so it's able to generate
		$rule->setCouponCodeGenerator($generator);
		$rule->setCouponType( Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO );
		
		// Get as many coupons as you required
		$count = !empty($parameters['count'])? (int) $parameters['count'] : 1;
		$codes = array();
		for( $i = 0; $i < $count; $i++ ){
		  $coupon = $rule->acquireCoupon();
		  $coupon->setUsageLimit(1);
		  $coupon->setTimesUsed(0);
		  $coupon->setType(1);
		  $coupon->save();
		  $code = $coupon->getCode();
		  $codes[] = $code;
		}
		
		echo "<pre>";
		print_r($codes);
		echo "</pre>";

        $this->renderLayout();
    }
	*/
}