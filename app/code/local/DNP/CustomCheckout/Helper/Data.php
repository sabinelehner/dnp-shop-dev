<?php

class DNP_CustomCheckout_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getOrderHistoryUrl() {
        return $this->_getUrl('sales/order/history');
    }

}
?>