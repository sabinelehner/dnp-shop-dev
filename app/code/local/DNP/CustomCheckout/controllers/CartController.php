<?php

/**
 * Shopping cart controller
 */
require_once 'Mage/Checkout/controllers/CartController.php';

class DNP_CustomCheckout_CartController extends Mage_Checkout_CartController {

    /**
     * Initialize coupon
     */
    public function couponPostAction()
    {
        /**
         * No reason continue with empty shopping cart
         */
        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            $this->_goBack();
            return;
        }

        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            $this->_goBack();
            return;
        }

        try {
            $codeLength = strlen($couponCode);
            $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;

            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode($isCodeLengthValid ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($codeLength) {
                if ($isCodeLengthValid && $couponCode == $this->_getQuote()->getCouponCode()) {
                    $this->_getSession()->addSuccess(
                        $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                    $this->_getSession()->setCartCouponCode($couponCode);
                } else {
					# DNP Special: give special error message if customer group is "Dr. Hauser"
					if ($this->_getCart()->getCustomerSession()->getCustomerGroupId() && ($this->_getCart()->getCustomerSession()->getCustomerGroupId() == 5)) {
						$this->_getSession()->addError(
							$this->__('Sorry - you are not able to use this coupon code because you already get a group discount.')
						);
					} else {
						$this->_getSession()->addError(
							$this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode))
						);
					}
                }
            } else {
                $this->_getSession()->addSuccess($this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }

        $this->_goBack();
    }
	
}