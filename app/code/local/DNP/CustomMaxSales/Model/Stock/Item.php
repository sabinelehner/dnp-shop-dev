<?php	
	
class DNP_CustomMaxSales_Model_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item 
{
    public function getMaxSaleQty() {
		
		$storeId = Mage::app()->getStore()->getStoreId();
		
		$maxQuantity = Mage::getResourceModel('catalog/product')->getAttributeRawValue($this->getProduct()->getId(), 'max_quantity', $storeId);
		
		/*
		Mage::log('---', null, 'dnp-log.log');
		Mage::log('storeId: ' . $storeId, null, 'dnp-log.log');
		Mage::log('maxQuantity: ' . $maxQuantity, null, 'dnp-log.log');
		Mage::log('product-id: ' . $this->getProduct()->getId(), null, 'dnp-log.log');
		Mage::log('maxQuantity: ' . $this->getProduct()->getData('max_quantity'), null, 'dnp-log.log');
		*/
				
        #if ($this->getProduct()->getData('max_quantity') && ($this->getProduct()->getData('max_quantity') > 0)) {
        if ($maxQuantity && ($maxQuantity > 0)) {
        	#return $this->getProduct()->getData('max_quantity');
			return $maxQuantity;
        }
		
        return parent::getMaxSaleQty();
    }
	
}
