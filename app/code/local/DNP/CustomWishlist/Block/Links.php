<?php
class DNP_CustomWishlist_Block_Links extends Mage_Wishlist_Block_Links {
	
    protected function _createLabel($count)
    {
        if ($count > 1) {
            return $this->__('My Wishlist<span class="wishlist_items"> (<span id="wishlist_item_count">%d</span> item<span id="wishlist_item_pl">s</span>)</span>', $count);
        } else if ($count == 1) {
            return $this->__('My Wishlist<span class="wishlist_items"> (<span id="wishlist_item_count">%d</span> item)</span>', $count);
        } else {
            return $this->__('My Wishlist');
        }
    }
	
    protected function _toHtml()
    {
        if ($this->helper('wishlist')->isAllow()) {
            $text = $this->_createLabel($this->_getItemCount());
            $this->_label = $text;
            $this->_title = strip_tags($text);
            $this->_url = $this->getUrl('wishlist');
			return '<a href="' . $this->_url . '">' . $this->_label . '</a>';
            #return parent::_toHtml();
        }
        return '';
    }
	
}
?>