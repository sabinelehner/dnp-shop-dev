<?php
class DNP_IPSwitcher_Helper_Data extends Mage_Core_Helper_Abstract
{
    const DEFAULT_STORE = 'English';

    /**
     * countries to store relation
     * default is English
     * @var array
     */
    protected $_countryToStore = array(
        'FR' => 'French',
        'BE' => 'French',
        'CH' => 'French',
        'DE' => 'German',
        'AT' => 'German',
        'UK' => 'English',
        'US' => 'English',
        'UA' => 'English',
        'CN' => 'English',
        'JP' => 'English'
    );

    /**
     * get store view name by country
     * @param $country
     * @return bool
     */
    public function getStoreByCountry($country)
    {
        if (isset($this->_countryToStore[$country])) {
            return $this->_countryToStore[$country];
        }
        return self::DEFAULT_STORE;
    }
	
    public function getJsBasedOnCountry($country)
    {
        return 'path-to-file/file1.js';
		
        if (Mage::getStoreConfigFlag('mymodule/settings/enable')) {
            return 'path-to-file/file1.js';
        }
        else {
            return 'path-to-file/file2.js';
        }
    }
}
?>