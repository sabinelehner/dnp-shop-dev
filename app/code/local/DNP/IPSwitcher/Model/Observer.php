<?php
class DNP_IPSwitcher_Model_Observer {
	
	private $disallowedCountries = array('CH');
		
    /**
     * redirects customer to store view based on GeoIP
     * @param $event
     */
    public function controllerActionPostdispatch($event)
    {
        $cookie = Mage::getSingleton('core/cookie');
		
        #if ($cookie->get('geoip_processed') != 1) {
            $geoIPCountry = Mage::getSingleton('geoip/country');
            $countryCode = $geoIPCountry->getCountry();
            $cookie->set('geoip_country', $countryCode, time() + 86400, '/');
			
            if (in_array($countryCode, $this->disallowedCountries)) {
            	$this->appendJS($countryCode);
            }			
			
			/*
            if ($countryCode) {
                $storeName = Mage::helper('ipswitcher')->getStoreByCountry($countryCode);
                if ($storeName) {
                    $store = Mage::getModel('core/store')->load($storeName, 'name');
                    if ($store->getName() != Mage::app()->getStore()->getName()) {
                        $event->getControllerAction()->getResponse()->setRedirect($store->getCurrentUrl(false));
                    }
                }
            }
			*/
			
            #$cookie->set('geoip_processed', '1', time() + 86400, '/');
        #}
			
    }

    private function appendJS($countryCode) {
        $layout = Mage::app()->getLayout();
        $block = $layout->createBlock('core/text');
		$partnerLink = '';
		
		switch($countryCode) {
			case 'CH':
				$partnerLink = 'http://www.fourtwenty.ch/';
				break;
		}
		
        $block->setText(
        '<script type="text/javascript">
			jQuery(window).load(function(){
				if (document.cookie.indexOf("CountryInfoShown=true") < 0) {
					jQuery("#partner-website").attr("href", "' . $partnerLink . '");
					jQuery("#countryInfo").modal("show");
					document.cookie = "CountryInfoShown=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
				}
			});
        </script>'
        );        
        $layout->getBlock('head')->append($block);  
    }
	
}
