<?php
class DNP_MailTransport_Model_Core_Email_Template extends Mage_Core_Model_Email_Template {
	
	const MODULE_SETTINGS_PATH = 'dnp_mail_transport';
	
	public function send($email, $name = null, array $variables = array()) {
		
		#echo "send it via extension";
		#exit();
		
        if (!$this->isValidForSend()) {
            Mage::logException(new Exception('This letter cannot be sent.')); // translation is intentionally omitted
            return false;
        }

        $emails = array_values((array)$email);
        $names = is_array($name) ? $name : (array)$name;
        $names = array_values($names);
        foreach ($emails as $key => $email) {
            if (!isset($names[$key])) {
                $names[$key] = substr($email, 0, strpos($email, '@'));
            }
        }

        $variables['email'] = reset($emails);
        $variables['name'] = reset($names);

        $this->setUseAbsoluteLinks(true);
        $text = $this->getProcessedTemplate($variables, true);
        $subject = $this->getProcessedTemplateSubject($variables);

        $setReturnPath = Mage::getStoreConfig(self::XML_PATH_SENDING_SET_RETURN_PATH);
        switch ($setReturnPath) {
            case 1:
                $returnPathEmail = $this->getSenderEmail();
                break;
            case 2:
                $returnPathEmail = Mage::getStoreConfig(self::XML_PATH_SENDING_RETURN_PATH_EMAIL);
                break;
            default:
                $returnPathEmail = null;
                break;
        }

        if ($this->hasQueue() && $this->getQueue() instanceof Mage_Core_Model_Email_Queue) {
            /** @var $emailQueue Mage_Core_Model_Email_Queue */
            $emailQueue = $this->getQueue();
            $emailQueue->setMessageBody($text);
            $emailQueue->setMessageParameters(array(
                    'subject'           => $subject,
                    'return_path_email' => $returnPathEmail,
                    'is_plain'          => $this->isPlain(),
                    'from_email'        => $this->getSenderEmail(),
                    'from_name'         => $this->getSenderName(),
                    'reply_to'          => $this->getMail()->getReplyTo(),
                    'return_to'         => $this->getMail()->getReturnPath(),
                ))
                ->addRecipients($emails, $names, Mage_Core_Model_Email_Queue::EMAIL_TYPE_TO)
                ->addRecipients($this->_bccEmails, array(), Mage_Core_Model_Email_Queue::EMAIL_TYPE_BCC);
            $emailQueue->addMessageToQueue();

            return true;
        }

        ini_set('SMTP', Mage::getStoreConfig('system/smtp/host'));
        ini_set('smtp_port', Mage::getStoreConfig('system/smtp/port'));

        $mail = $this->getMail();

        if ($returnPathEmail !== null) {
            $mailTransport = new Zend_Mail_Transport_Sendmail("-f".$returnPathEmail);
            Zend_Mail::setDefaultTransport($mailTransport);
        }

        foreach ($emails as $key => $email) {
            $mail->addTo($email, '=?utf-8?B?' . base64_encode($names[$key]) . '?=');
        }

        if ($this->isPlain()) {
            $mail->setBodyText($text);
        } else {
            $mail->setBodyHTML($text);
        }

        $mail->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
        $mail->setFrom($this->getSenderEmail(), $this->getSenderName());
		
		try {
			$systemStoreConfig = Mage::getStoreConfig('system');
			$emailSmtpConf = array(
				'auth' => strtolower($systemStoreConfig[self::MODULE_SETTINGS_PATH]['auth']),
				#'ssl' => strtolower($systemStoreConfig[self::MODULE_SETTINGS_PATH]['ssl']),
				'username' => $systemStoreConfig[self::MODULE_SETTINGS_PATH]['username'],
				'password' => $systemStoreConfig[self::MODULE_SETTINGS_PATH]['password']
			);
			$smtp = 'smtp.gmail.com';
			
			if ($systemStoreConfig[self::MODULE_SETTINGS_PATH]['smtphost']) {
				$smtp = strtolower($systemStoreConfig[self::MODULE_SETTINGS_PATH]['smtphost']);
			}
			$transport = new Zend_Mail_Transport_Smtp($smtp, $emailSmtpConf);
			$mail->send($transport);
			$this->_mail = null;
		}
		catch (Exception $ex) {
			
			//Zend_Debug::dump($systemStoreConfig[self::MODULE_SETTINGS_PATH]);
			//Zend_Debug::dump($ex->getMessage()); exit;
			
			try {
				$mail->send(); /* Try regular email send if the one with $transport fails */
				$this->_mail = null;
			}
			catch (Exception $ex) {
				$this->_mail = null;
				
				//Zend_Debug::dump($systemStoreConfig[self::MODULE_SETTINGS_PATH]);
				//Zend_Debug::dump($ex->getMessage()); exit;
				
				Mage::logException($ex);
				return false;
			}
			Mage::logException($ex);
			return false;
		}

		return true;
	}
} 