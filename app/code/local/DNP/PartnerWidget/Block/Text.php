<?php   
class DNP_PartnerWidget_Block_Text extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface
{ 
	protected function _toHtml()
	{
		/*
		DEV WaWi
		https://devwawi.deepnatureproject.com/www/index.php?module=api&action=ExportVorlageGet&hash=f7777f3f21c8b05e12f6bc10ae12d0bf8ec9687a&id=4
		$wawiurl = 'https://devwawi.deepnatureproject.com';
		$hash = 'ed9ac19c2ff5df25827bdf7cf2d0b5196d9c313f';
		*/
		$wawiurl = 'https://wawi.deepnatureproject.com';
		$hash = 'cb8a3fbd652d7fa865f1da5ebc357d5596e8a563';
		
		$domain = 'https://deepnatureproject.com';
		$key = 'EXsl4ywgDR5mR3sTGwws9Ikl2ocSKZlvhxoFTv8Au3MaV5UqrEIJORVF5PmFNy';
		$date = gmdate('dmY');
		$hash = "";		
		for ($i = 0; $i <= 200; $i++) {
			$hash = sha1($hash . $key . $domain . $date);
		}  

		$url = $wawiurl.'/www/index.php?module=api&action=ExportVorlageGet&hash='.$hash.'&id=2';
		
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST'
			),
		);
		$context = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		$result = str_replace('&', '&amp;', $result);
		
		$xml = simplexml_load_string($result);
		$html = '';
		
		#print_r($xml);
		
		if ($xml->status->messageCode != '1') {
			$html = $this->__('An error occured - please contact the site administrator.');
		} else {
			
			foreach ($xml->xml->items->item as $address) {
				$html .= "<h2>" . Mage::app()->getLocale()->getCountryTranslation($address->land) . "</h2>";
				$html .= "<p><strong>" . $address->name . "</strong></p>";
				
				$html .= "<p>";
				$html .= $address->strasse . "<br />";
				$html .= $address->land . " - " . $address->plz . " " . $address->ort . "<br />";
				$html .= $address->telefon . "<br />";
				$html .= "<a href=\"mailto:" . $address->email . "\" target=\"_blank\">" . $address->email . "</a><br />";
				$html .= "<a href=\"http://" . $address->internetseite . "\" target=\"_blank\">" . $address->internetseite . "</a><br />";
				$html .= "</p>";
				$html .= "<p>&nbsp;</p>";
			}
			
		}
	
		return $html;
	}

}