<?php
require_once 'Mage/Review/controllers/ProductController.php';

class DNP_ReviewEmail_ProductController extends Mage_Review_ProductController
{
    protected function _cropReviewData(array $reviewData)
    {
        $croppedValues = array();
        $allowedKeys = array_fill_keys(array('detail', 'title', 'nickname', 'email'), true);

        foreach ($reviewData as $key => $value) {
            if (isset($allowedKeys[$key])) {
                $croppedValues[$key] = $value;
            }
        }

        return $croppedValues;
    }
}