<?php
$installer = $this;
$installer->startSetup();
#$installer->run("ALTER TABLE review_detail ADD COLUMN email VARCHAR(255) NULL");
$installer->getConnection()
    ->addColumn($installer->getTable('review_detail'),
        'email',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
            'length' => 255,
            'nullable' => true,
            'default' => '',
            #'comment' => 'Breifly describe the new column'
        )
    );
$installer->endSetup();