<?php
class DNP_Sales_Model_Quote extends Mage_Sales_Model_Quote
{	

	public function _beforeSave()
	{
		$addresses = $this->getAllAddresses();
			
		if (count($addresses) > 2) {
		
			$custID = $addresses[0]->getCustomerId();
			Mage::log('--- Mage_Sales_Model_Quote ---', null, 'dnp-log.log');
			Mage::log('check - double address for customer ' . $custID, null, 'dnp-log.log');
			
			for ($i = 2; $i < count($addresses); $i++) {
				$address = $addresses[$i];
				$address->isDeleted(true);
			}
		}
		
		parent::_beforeSave();
	}
	
}
