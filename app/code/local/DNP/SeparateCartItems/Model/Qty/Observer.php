<?php
class DNP_SeparateCartItems_Model_Qty_Observer {
	
    public function separate_items(Varien_Event_Observer $observer) {
		
		$website = Mage::app()->getStore()->getWebsiteId();
		
		#Mage::log('--- DNP_SeparateCartItems_Model_Qty_Observer ---', null, 'dnp-log.log');
		#Mage::log('website: ' . $website, null, 'dnp-log.log');
		
		# im Wiederverkaufsshop wird jedes Produkt als separate Position im Warenkorb gelistet
		if ($website == 5) {		
			$quote_item = $observer->getQuoteItem();
			$product = $quote_item->getProduct(); 
			
			$categories = $product->getCategoryIds();
		
			#Mage::log('--- DNP_SeparateCartItems_Model_Qty_Observer ---', null, 'dnp-log.log');
			#Mage::log('product-cats: ' . implode(',', $categories), null, 'dnp-log.log');
			
			# Werbematerial darf nicht separat in den Warenkorb gelegt werden (Höchstbestellmengen!!!)
			if (!in_array(20, $categories)) {
				$data['microtime'] = microtime(true);
				$product->addCustomOption('do_not_merge', serialize($data));		
				$quote_item->addOption($product->getCustomOption('do_not_merge'));
			}
		}
    }
	
}
