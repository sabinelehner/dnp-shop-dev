<?php
class DNP_WebForms_Model_Results extends VladimirPopov_WebForms_Model_Results
{	

    public function getEmailSubject($recipient = 'admin')
    {
        /** @var VladimirPopov_WebForms_Model_Webforms $webform */
        $webform = $this->getWebform();
        $webform_name = $webform->getName();
        $store_name = Mage::app()->getStore($this->getStoreId())->getFrontendName();

        //get default subject for admin
        $subject = Mage::helper('webforms')->__("'%s' submitted", $webform_name);

        //get subject for customer
        if ($recipient == 'customer') {
            $subject = Mage::helper('webforms')->__("You have submitted '%s' form on %s website", $webform_name, $store_name);
        }

        //iterate through fields and build subject
        $subject_array = array();
        $fields_to_fieldsets = $webform->getFieldsToFieldsets(true);
        $logic_rules = $webform->getLogic();
        $this->addFieldArray();
        /** @var VladimirPopov_WebForms_Model_Fields $field */
        foreach ($fields_to_fieldsets as $fieldset) {
            foreach ($fieldset['fields'] as $field) {

                $target_field = array("id" => 'field_' . $field->getId(), 'logic_visibility' => $field->getData('logic_visibility'));
                $field_visibility = $webform->getLogicTargetVisibility($target_field, $logic_rules, $this->getData('field'));

                if ($field_visibility && $field->getEmailSubject()) {
                    foreach ($this->getData() as $key => $value) {
                        if ($key == 'field_' . $field->getId() && $value) {
                            $subject_array[] = $field->prepareResultValue($value);
                        }
                    }
                }
            }
        }

        if (count($subject_array) > 0) {
            $subject = implode(" / ", $subject_array);
        }
        return $subject;
    }

    public function toHtml($recipient = 'admin', $options = array())
    {
        /** @var VladimirPopov_WebForms_Model_Webforms $webform */
        $webform = Mage::getModel('webforms/webforms')
            ->setStoreId($this->getStoreId())
            ->load($this->getWebformId());

        $this->addFieldArray(true);

        if (!isset($options['header'])) {
            $options['header'] = $webform->getAddHeader();
        }
        if (!isset($options['skip_fields'])) {
            $options['skip_fields'] = array();
        }

        $html = "";
        $store_group = Mage::app()->getStore($this->getStoreId())->getFrontendName();
        $store_name = Mage::app()->getStore($this->getStoreId())->getName();
        if ($recipient == 'admin') {
            if ($store_group)
                $html .= Mage::helper('webforms')->__('Store group') . ": " . $store_group . "<br>";
            if ($store_name)
                $html .= Mage::helper('webforms')->__('Store name') . ": " . $store_name . "<br>";
            $html .= Mage::helper('webforms')->__('Customer') . ": " . $this->getCustomerName() . "<br>";
            $html .= Mage::helper('webforms')->__('Email') . ": " . implode(',', $this->getCustomerEmail()) . "<br>";
            $html .= Mage::helper('webforms')->__('IP') . ": " . $this->getIp() . "<br>";
        }
        $html .= Mage::helper('webforms')->__('Date') . ": " . Mage::helper('core')->formatDate($this->getCreatedTime(), 'medium', true) . "<br>";
        $html .= "<br>";

        $head_html = "";
        if ($options['header']) $head_html = $html;

        $html = "";

        $logic_rules = $webform->getLogic();

        $fields_to_fieldsets = $webform
            ->getFieldsToFieldsets(true);
        foreach ($fields_to_fieldsets as $fieldset_id => $fieldset) {

            $k = false;
            $field_html = "";

            $target_fieldset = array("id" => 'fieldset_' . $fieldset_id, 'logic_visibility' => $fieldset['logic_visibility']);
            $fieldset_visibility = $webform->getLogicTargetVisibility($target_fieldset, $logic_rules, $this->getData('field'));

            if ($fieldset_visibility) {
                /** @var VladimirPopov_WebForms_Model_Fields $field */
                foreach ($fieldset['fields'] as $field) {
                    $target_field = array("id" => 'field_' . $field->getId(), 'logic_visibility' => $field->getData('logic_visibility'));
                    $field_visibility = $webform->getLogicTargetVisibility($target_field, $logic_rules, $this->getData('field'));
                    $value = $this->getData('field_' . $field->getId());
                    if ($field->getType() == 'html')
                        $value = $field->getValue();
                    if (is_string($value)) $value = trim($value);
                    if ($value && $field_visibility) {
                        if (!in_array($field->getType(), $options['skip_fields']) && $field->getResultDisplay() != 'off') {
                            $field_name = $field->getName();
                            if (strlen(trim($field->getResultLabel())) > 0)
                                $field_name = $field->getResultLabel();
                            if ($field->getResultDisplay() != 'value') $field_html .= '<b>' . $field_name . '</b><br>';
                            $filename = $value;
                            switch ($field->getType()) {
                                case 'date':
                                case 'datetime':
                                case 'date/dob':
                                    $value = $field->formatDate($value);
                                    break;
                                case 'stars':
                                    $value = $value . ' / ' . $field->getStarsCount();
                                    break;
                                case 'file':
                                    if (strlen($value) > 1)
                                        $value = '<a href="' . $this->getDownloadLink($field->getId(), $filename) . '">' . $filename . '</a> <small>[' . $this->getFileSizeText($field->getId(), $filename) . ']</small>';
                                    break;
                                case 'image':
                                    if (strlen($value) > 1)
                                        $value = '<a style="text-decoration:none" href="' . $this->getDownloadLink($field->getId(), $filename) . '"><img src="' . $this->getThumbnail($field->getId(), $filename, Mage::getStoreConfig('webforms/images/email_thumbnail_width'), Mage::getStoreConfig('webforms/images/email_thumbnail_height')) . '"/></a>';
                                    break;
                                case 'select/contact':
                                    $contact = $field->getContactArray($value);
                                    if (!empty($contact["name"])) $value = $contact["name"];
                                    break;
                                case 'html':
                                    $value = trim($field->getValue('html'));
                                    break;
                                case 'country':
                                    $country_name = Mage::app()->getLocale()->getCountryTranslation($value);
                                    if ($country_name) $value = $country_name;
                                    break;
                                case 'subscribe':
                                    if ($value) $value = Mage::helper('core')->__('Yes');
                                    else $value = Mage::helper('core')->__('No');
                                    break;
                                default :
                                    $value = nl2br(htmlspecialchars($value));
                                    break;
                            }
                            $k = true;
                            $value = new Varien_Object(array('html' => $value, 'value' => $this->getData('field_' . $field->getId())));
                            Mage::dispatchEvent('webforms_results_tohtml_value', array('field' => $field, 'value' => $value, 'result' => $this));
                            $field_html .= $value->getHtml() . "<br><br>";
                        }
                    }

                }
            }
            if (!empty($fieldset['name']) && $k && $fieldset['result_display'] == 'on')
                $field_html = '<h2>' . $fieldset['name'] . '</h2>' . $field_html;
            $html .= $field_html;
        }
        return $head_html . $html;

    }

}

