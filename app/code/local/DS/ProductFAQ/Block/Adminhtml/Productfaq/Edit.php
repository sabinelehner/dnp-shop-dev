<?php
class DS_ProductFAQ_Block_Adminhtml_productfaq_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_controller = 'productfaq';

        $this->_updateButton('save', 'label', Mage::helper('ds_productfaq')->__('Save Answer'));
        $this->_updateButton('delete', 'label', Mage::helper('ds_productfaq')->__('Delete Question'));

    }

    public function getHeaderText()
    {
        if( Mage::registry('productfaq') && Mage::registry('productfaq')->getId() ) {
            return Mage::helper('ds_productfaq')->__("Edit Answer", $this->htmlEscape(Mage::registry('productfaq')->getTitle()));
        } else {
            return Mage::helper('ds_productfaq')->__('New Question');
        }
    }
     
    protected function _prepareLayout()
    { 
        if ($this->_blockGroup && $this->_controller && $this->_mode) {
            $this->setChild('form', $this->getLayout()->createBlock('ds_productfaq/adminhtml_productfaq_edit_form'));
        }
        return parent::_prepareLayout();
    }
}
