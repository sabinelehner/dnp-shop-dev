<?php
class DS_ProductFAQ_Block_Adminhtml_productfaq_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('productfaq_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ds_productfaq')->__('General'));
    }
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }
   

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('ds_productfaq')->__('General'),
            'title'     => Mage::helper('ds_productfaq')->__('General'),
            'content'   => $this->getLayout()->createBlock('ds_productfaq/adminhtml_productfaq_edit_tab_form')->toHtml(),
        ));
       
        return parent::_beforeToHtml();
    }
}
