<?php
class DS_ProductFAQ_Helper_Data extends Mage_Core_Helper_Abstract
{	
	public function isActiveCaptcha() {
		return Mage::getStoreConfig('ds_productfaq/general/enable_captcha', Mage::app()->getStore()->getId());
	}
}
