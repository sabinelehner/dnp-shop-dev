<?php
class DS_ProductFAQ_Model_Mysql4_Productfaq extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {       
        $this->_init('ds_productfaq/productfaq','productfaq_id');
    } 
    
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (!$object->getCreatedOn()) {
            $object->setCreatedOn($this->formatDate(time()));
        }
        
        $object->setUpdatedOn($this->formatDate(time()));
        parent::_beforeSave($object);
    }
    
    
    
    
    
}

