<?php
class DS_ProductFAQ_Model_Productfaq extends Mage_Core_Model_Abstract 
{
    protected function _construct()
    {
        $this->_init('ds_productfaq/productfaq');
    }
        
    public function loadExtra($id)
    {
        $collection = $this->getCollection()
                            ->joinProducts()
                            ->joinStore()
                            ->addFieldToFilter('productfaq_id', $id);
        
        if ($collection->getSize()) {
            return $collection->getFirstItem();
        }  
        return false;
    }
}
