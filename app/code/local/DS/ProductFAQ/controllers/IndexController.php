<?php
class DS_ProductFAQ_IndexController extends Mage_Core_Controller_Front_Action
{
   
   const CONFIG_ENABLE_CAPTCHA = 'ds_productfaq/general/enable_captcha';
   const CONFIG_SEND_NOTIFICATION_EMAIL = 'ds_productfaq/general/send_notification';
   const CONFIG_SEND_NOTIFICATION_EMAIL_TO = 'ds_productfaq/general/notification_email';
   
   const XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY  = 'default/ds_productfaq/emails/email_identity';
   const XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE  = 'product_faq_notification';
   
   public function askQuestionAction()
   {
        $post = $this->getRequest()->getPost();
        if ($post) {
			           
            try {
				//validate captcha 
				if(!$this->isValidCaptcha($post)) {
					//throw new Exception();
					Mage::getSingleton('catalog/session')->addError(Mage::helper('ds_productfaq')->__('Please enter valid captcha text.'));
					return $this->_redirectReferer();
				}else{
			
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['question']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }
                
                $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $productfaq = Mage::getModel('ds_productfaq/productfaq');
                $productfaq->setData($post);
                $productfaq->save();
                
                
                // for sending notification email
                
                $sendNotificationEmail = Mage::getStoreConfig(self::CONFIG_SEND_NOTIFICATION_EMAIL);
               
                               
                if ($sendNotificationEmail) {  
                    $product = Mage::getModel('catalog/product')->load($productfaq->getProductId());
                              
                    $emailData = array();
                    $emailData['to_email'] = Mage::getStoreConfig(self::CONFIG_SEND_NOTIFICATION_EMAIL_TO);
                    $emailData['to_name'] =  Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY);
                    $emailData['email'] = array(
                        'product_name' => $product->getName(),
                        'store_id' => $productfaq->getStoreId(),
                        'question' => $productfaq->getQuestion(),
                        'date_posted' => Mage::helper('core')->formatDate($productfaq->getCreatedOn(), 'long'), 
                    ); 
		            $result = $this->sendEmail($emailData);
		            
		            if(!$result) {
		                Mage::throwException($this->__('Cannot send email'));
		            }
		        }      
                
                Mage::getSingleton('catalog/session')->addSuccess(Mage::helper('ds_productfaq')->__('Your question has been submitted successfully, soon you will get a response.'));
                
                return $this->_redirectReferer();
			   }
            } catch (Exception $e) {               

                Mage::getSingleton('catalog/session')->addError(Mage::helper('ds_productfaq')->__('Unable to submit your question. Please, try again later'.$e->getMessage()));
                
                return $this->_redirectReferer();
            }

        } else {
            return $this->_redirectReferer();
        }
    }
    
	public function imagecaptchaAction() {
		require_once(Mage::getBaseDir('lib') . DS .'captcha'. DS .'class.simplecaptcha.php');
		$config['BackgroundImage'] = Mage::getBaseDir('lib') . DS .'captcha'. DS . "white.png";
		$config['BackgroundColor'] = "FF0000";
		$config['Height']=30;
		$config['Width']=100;
		$config['Font_Size']=23;
		$config['Font']= Mage::getBaseDir('lib') . DS .'captcha'. DS . "ARLRDBD.TTF";
		$config['TextMinimumAngle']=0;
		$config['TextMaximumAngle']=0;
		$config['TextColor']='000000';
		$config['TextLength']=4;
		$config['Transparency']=80;
		$captcha = new SimpleCaptcha($config);
		$_SESSION['captcha_code'] = $captcha->Code;
	}
	
	public function refreshcaptchaAction() {
		$result = Mage::getModel('core/url')->getUrl('*/*/imageCaptcha/') .  now();
		echo $result;
	}
	
	protected function isValidCaptcha($data) {
		//check whether captcha is enabled in admin
		$showCaptcha = Mage::getStoreConfig(self::CONFIG_ENABLE_CAPTCHA);
		//if not disabled then no need to check further, just return true
		if(!$showCaptcha) return true; 
		
		if(!isset($_SESSION['captcha_code'])) {
			return false;
		}
		
		$captchaCode = trim($_SESSION['captcha_code']);
		$captchaText = trim($data['captcha_text']);
		if(strtolower($captchaText) != strtolower($captchaCode)) {
			return false;
		}
		return true;
		
	}

    private function sendEmail($data)
	{	
		
		$storeID = $data['email']['store_id'];
		
		$translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $result = Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeID));
        
        $result->sendTransactional(
                self::XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE,
                Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY),
                $data['to_email'],
                $data['to_name'],
                $data['email'],
                $storeID
               );
        
        $translate->setTranslateInline(true);
        
        return $result;
	}
	
	
   
}
