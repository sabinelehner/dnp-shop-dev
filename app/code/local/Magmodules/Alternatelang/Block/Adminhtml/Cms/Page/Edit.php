<?php
/**
 * Magmodules.eu - http://www.magmodules.eu.
 *
 * NOTICE OF LICENSE
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.magmodules.eu/MM-LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magmodules.eu so we can send you a copy immediately.
 *
 * @category      Magmodules
 * @package       Magmodules_Alternatelang
 * @author        Magmodules <info@magmodules.eu>
 * @copyright     Copyright (c) 2017 (http://www.magmodules.eu)
 * @license       https://www.magmodules.eu/terms.html  Single Service License
 */

if (Mage::helper('core')->isModuleEnabled('Bubble_CmsTree')) {
    class Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Edit_Abstract
        extends Bubble_CmsTree_Block_Adminhtml_Cms_Page_Edit
    {
    };
} else {
    class Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Edit_Abstract
        extends Mage_Adminhtml_Block_Cms_Page_Edit
    {
    };
}

class Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Edit
    extends Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Edit_Abstract
{

    /**
     * Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();
        if (Mage::getStoreConfig('alternatelang/config/cms_categories')) {
            $this->_formScripts[] = " 
				category_new();
			";
        }
    }

}
