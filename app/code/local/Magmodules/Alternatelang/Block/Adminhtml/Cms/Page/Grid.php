<?php
/**
 * Magmodules.eu - http://www.magmodules.eu.
 *
 * NOTICE OF LICENSE
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.magmodules.eu/MM-LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magmodules.eu so we can send you a copy immediately.
 *
 * @category      Magmodules
 * @package       Magmodules_Alternatelang
 * @author        Magmodules <info@magmodules.eu>
 * @copyright     Copyright (c) 2017 (http://www.magmodules.eu)
 * @license       https://www.magmodules.eu/terms.html  Single Service License
 */

if (Mage::helper('core')->isModuleEnabled('Hackathon_MultistoreBlocks')) {
    class Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Grid_Abstract
        extends Hackathon_MultistoreBlocks_Block_Adminhtml_Cms_Block_Grid_Page
    {
    };
} else {
    class Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Grid_Abstract
        extends Mage_Adminhtml_Block_Cms_Page_Grid
    {
    };
}

class Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Grid
    extends Magmodules_Alternatelang_Block_Adminhtml_Cms_Page_Grid_Abstract
{

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        if (Mage::getStoreConfig('alternatelang/config/cms_categories')) {
            $collection = Mage::getModel('cms/page')->getCollection()
                ->distinct(true)->addFieldToSelect('alternate_category')->setOrder('title', 'ASC');
            $collection->setFirstStoreFlag(true);
            $alternateCategoryOptions = '';

            foreach ($collection as $option) {
                if ($option->getAlternateCategory()) {
                    $alternateCategoryOptions[$option->getAlternateCategory()] = $option->getAlternateCategory();
                }
            }

            $this->addColumnAfter(
                'alternate_category', array(
                'header'  => Mage::helper('alternatelang')->__('Category'),
                'index'   => 'alternate_category',
                'type'    => 'options',
                'options' => $alternateCategoryOptions,
                ), 'update_time'
            );
        }

        return parent::_prepareColumns();
    }

}