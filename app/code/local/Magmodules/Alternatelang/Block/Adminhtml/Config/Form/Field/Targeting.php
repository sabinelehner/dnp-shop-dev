<?php
/**
 * Magmodules.eu - http://www.magmodules.eu.
 *
 * NOTICE OF LICENSE
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.magmodules.eu/MM-LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magmodules.eu so we can send you a copy immediately.
 *
 * @category      Magmodules
 * @package       Magmodules_Alternatelang
 * @author        Magmodules <info@magmodules.eu>
 * @copyright     Copyright (c) 2017 (http://www.magmodules.eu)
 * @license       https://www.magmodules.eu/terms.html  Single Service License
 */

class Magmodules_Alternatelang_Block_Adminhtml_Config_Form_Field_Targeting
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    protected $_renders = array();

    /**
     * Magmodules_Alternatelang_Block_Adminhtml_Config_Form_Field_Targeting constructor.
     */
    public function __construct()
    {
        $layout = Mage::app()->getFrontController()->getAction()->getLayout();
        $rendererStore = $layout->createBlock(
            'alternatelang/adminhtml_config_form_renderer_select', '',
            array('is_render_to_js_template' => true)
        );
        $rendererStore->setOptions(Mage::getModel('alternatelang/source_store')->toOptionArray());
        $rendererGroup = $layout->createBlock(
            'alternatelang/adminhtml_config_form_renderer_select', '',
            array('is_render_to_js_template' => true)
        );
        $rendererGroup->setOptions(Mage::getModel('alternatelang/source_group')->toOptionArray());

        $this->addColumn(
            'store_id', array(
            'label'    => Mage::helper('alternatelang')->__('Storefront'),
            'style'    => 'width:120px',
            'renderer' => $rendererStore
            )
        );

        $this->addColumn(
            'language_code', array(
            'label' => Mage::helper('alternatelang')->__('Language Code'),
            'style' => 'width:80px',
            )
        );

        $this->addColumn(
            'group', array(
            'label'    => Mage::helper('alternatelang')->__('Group'),
            'style'    => 'width:100px',
            'renderer' => $rendererGroup
            )
        );

        $this->_renders['store_id'] = $rendererStore;
        $this->_renders['group'] = $rendererGroup;

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('alternatelang')->__('Add Option');
        parent::__construct();
    }

    /**
     * @param Varien_Object $row
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        foreach ($this->_renders as $key => $render) {
            $row->setData(
                'option_extra_attr_' . $render->calcOptionHash($row->getData($key)),
                'selected="selected"'
            );
        }
    }

}