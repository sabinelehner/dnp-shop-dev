<?php

/**
 * Magmodules.eu - http://www.magmodules.eu.
 *
 * NOTICE OF LICENSE
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.magmodules.eu/MM-LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magmodules.eu so we can send you a copy immediately.
 *
 * @category      Magmodules
 * @package       Magmodules_Alternatelang
 * @author        Magmodules <info@magmodules.eu>
 * @copyright     Copyright (c) 2017 (http://www.magmodules.eu)
 * @license       https://www.magmodules.eu/terms.html  Single Service License
 */
class Magmodules_Alternatelang_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * @return array|bool
     */
    public function getAlternateData()
    {
        if (Mage::getStoreConfig('alternatelang/general/enabled')) {
            $alternateUrls = array();
            $router = Mage::app()->getRequest()->getRouteName();
            $controller = Mage::app()->getRequest()->getControllerName();
            $storeId = Mage::app()->getStore()->getStoreId();
            $stores = $this->getConnectedStores($storeId);
            $alternateUrls['router'] = $router;
            $alternateUrls['stores'] = $stores;

            if (count($stores) > 1) {
                $cmsEnabled = Mage::getStoreConfig('alternatelang/config/cms');
                if (($router == 'cms') && $cmsEnabled && ($controller != 'index') && !Mage::registry('current_product')) {
                    $cmsCategory = Mage::getBlockSingleton('cms/page')->getPage()->getAlternateCategory();
                    $cmsId = Mage::getBlockSingleton('cms/page')->getPage()->getId();
                    foreach ($stores as $storeAlternate) {
                        $url = '';
                        $storeAlternateLanguageCode = $storeAlternate['language_code'];
                        $storeAlternate = Mage::getModel('core/store')->load($storeAlternate['store_id']);
                        if (Mage::getStoreConfig('alternatelang/config/cms', $storeAlternate->getId())) {
                            $page = Mage::getModel('cms/page')->setStoreId($storeAlternate->getId())->load($cmsId);
                            $cat = Mage::getModel('cms/page')->setStoreId($storeAlternate->getId())->load(
                                $cmsCategory,
                                'alternate_category'
                            );
                            $cmsUrlSuffix = $this->_getCmsUrlSuffix();
                            if ($page->getIdentifier()) {
                                $url = $storeAlternate->getBaseUrl() . $page->getIdentifier() . $cmsUrlSuffix;
                            }

                            if ($cat->getIdentifier()) {
                                $url = $storeAlternate->getBaseUrl() . $cat->getIdentifier() . $cmsUrlSuffix;
                            }
                        }

                        if ($url) {
                            if ($storeAlternateLanguageCode) {
                                $alternateUrls['urls'][$storeAlternateLanguageCode] = $url;
                            } else {
                                $storeLocale = substr(
                                    Mage::getStoreConfig(
                                        'general/locale/code',
                                        $storeAlternate->getId()
                                    ), 0, 2
                                );
                                $alternateUrls['urls'][$storeLocale] = $url;
                            }
                        }
                    }
                }

                $homepageEnabled = Mage::getStoreConfig('alternatelang/config/homepage');
                if (($router == 'cms') && $homepageEnabled && ($controller == 'index')) {
                    foreach ($stores as $storeAlternate) {
                        $storeAlternateLanguageCode = $storeAlternate['language_code'];
                        $storeAlternate = Mage::getModel('core/store')->load($storeAlternate['store_id']);
                        if (Mage::getStoreConfig('alternatelang/config/homepage', $storeAlternate->getId())) {
                            if ($storeAlternateLanguageCode) {
                                $alternateUrls['urls'][$storeAlternateLanguageCode] = $storeAlternate->getBaseUrl();
                            } else {
                                $storeLocale = substr(
                                    Mage::getStoreConfig(
                                        'general/locale/code',
                                        $storeAlternate->getId()
                                    ), 0, 2
                                );
                                $alternateUrls['urls'][$storeLocale] = $storeAlternate->getBaseUrl();
                            }
                        }
                    }
                }

                $productEnabled = Mage::getStoreConfig('alternatelang/config/product');
                if (($product = Mage::registry('current_product')) && $productEnabled) {
                    if (Mage::getStoreConfig('alternatelang/config/canonical_exclusive')) {
                        if (Mage::helper('catalog/product')->canUseCanonicalTag()) {
                            $params = array('_ignore_category' => true);
                            $canonicalUrl = Mage::getModel('catalog/product')->getUrlModel()->getUrl($product, $params);
                            $canonicalUrl = str_replace('?___SID=U', '', $canonicalUrl);
                            $currentUrl = $this->getCleanCurrentUrl();
                            if ($canonicalUrl != $currentUrl) {
                                $msg = $this->__('There is no alternate URL for this page as this URL is not the canonical URL.<br/>');
                                $msg .= $this->__('The canonical URL for this page is: %s <br/>', $canonicalUrl);
                                $msg .= $this->__('The requested URL for this page is: %s <br/>', $currentUrl);
                                $alternateUrls['errors'] = $msg;
                            }
                        }
                    }

                    foreach ($stores as $storeAlternate) {
                        $storeAlternateLanguageCode = $storeAlternate['language_code'];
                        $storeAlternate = Mage::getModel('core/store')->load($storeAlternate['store_id']);
                        if (Mage::getStoreConfig('alternatelang/config/product', $storeAlternate->getId())) {
                            if ($url = $this->getCoreProductUrl($product->getId(), $storeAlternate->getId())) {
                                $url = $storeAlternate->getBaseUrl() . $url;
                                if ($storeAlternateLanguageCode) {
                                    $alternateUrls['urls'][$storeAlternateLanguageCode] = $url;
                                } else {
                                    $storeLocale = substr(
                                        Mage::getStoreConfig(
                                            'general/locale/code',
                                            $storeAlternate->getId()
                                        ), 0, 2
                                    );
                                    $alternateUrls['urls'][$storeLocale] = $url;
                                }
                            }
                        }
                    }

                }

                $categoryEnabled = Mage::getStoreConfig('alternatelang/config/category');
                if (($category = Mage::registry('current_category')) && $categoryEnabled && (!Mage::registry('current_product'))) {
                    if (Mage::getStoreConfig('alternatelang/config/canonical_exclusive')) {
                        if (Mage::helper('catalog/category')->canUseCanonicalTag()) {
                            $currentUrl = $this->getCleanCurrentUrl();
                            $canonicalUrl = str_replace('?___SID=U', '', $category->getUrl());
                            if ($canonicalUrl != $currentUrl) {
                                $msg = $this->__('There is no alternate URL for this page as this URL is not the canonical URL.<br/>');
                                $msg .= $this->__('The canonical URL for this page is: %s <br/>', $canonicalUrl);
                                $msg .= $this->__('The requested URL for this page is: %s <br/>', $currentUrl);
                                $alternateUrls['errors'] = $msg;
                            }
                        }
                    }

                    foreach ($stores as $storeAlternate) {
                        $storeAlternateLanguageCode = $storeAlternate['language_code'];
                        $storeAlternate = Mage::getModel('core/store')->load($storeAlternate['store_id']);
                        if (Mage::getStoreConfig('alternatelang/config/category', $storeAlternate->getId())) {
                            if ($url = $this->getCoreCategoryUrl($category->getId(), $storeAlternate->getId())) {
                                $url = $storeAlternate->getBaseUrl() . $url;
                                if ($storeAlternateLanguageCode) {
                                    $alternateUrls['urls'][$storeAlternateLanguageCode] = $url;
                                } else {
                                    $storeLocale = substr(
                                        Mage::getStoreConfig(
                                            'general/locale/code',
                                            $storeAlternate->getId()
                                        ), 0, 2
                                    );
                                    $alternateUrls['urls'][$storeLocale] = $url;
                                }
                            }
                        }
                    }
                }

                if (is_array($alternateUrls)) {
                    return $alternateUrls;
                }
            }
        }

        return false;
    }

    /**
     * @param $storeId
     *
     * @return array
     */
    public function getConnectedStores($storeId)
    {
        $shops = @unserialize(Mage::getStoreConfig('alternatelang/targeting/shops'));
        $group = $this->getConnectedGroup($storeId, $shops);
        $stores = array();
        foreach ($shops as $shop) {
            if ($shop['group'] == $group) {
                $stores[] = array(
                    "store_id"      => $shop['store_id'],
                    "language_code" => $shop['language_code'],
                    "group"         => $shop['group']
                );
            }
        }

        return $stores;
    }

    /**
     * @param $storeId
     * @param $shops
     *
     * @return mixed
     */
    public function getConnectedGroup($storeId, $shops)
    {
        foreach ($shops as $shop) {
            if ($shop['store_id'] == $storeId) {
                $group = $shop['group'];
                break;
            }
        }

        if (isset($group)) {
            return $group;
        }

        return false;
    }

    /**
     * @return string
     */
    protected function _getCmsUrlSuffix()
    {
        $suffix = '';

        if (Mage::helper('core')->isModuleEnabled('Bubble_CmsTree')) {
            $suffix = Mage::helper('bubble_cmstree')->getUrlSuffix();
        }

        return $suffix;
    }

    /**
     * @return mixed
     */
    public function getCleanCurrentUrl()
    {
        $url = Mage::helper('core/url')->getCurrentUrl();
        $relace = array('?show-alternate=1', '?show-alternate', '&show-alternate', '&show-alternate=1');
        $url = str_replace($relace, '', $url);
        return trim($url);
    }

    /**
     * @param $productId
     * @param $storeId
     *
     * @return bool
     */
    public function getCoreProductUrl($productId, $storeId)
    {
        if ($this->checkProductVisibility($productId, $storeId)) {
            if ($category = Mage::registry('current_category')) {
                $categoryId = $category->getId();
            } else {
                $categoryId = '';
            }

            $coreUrl = Mage::getModel('core/url_rewrite');
            $idPath = sprintf('product/%d', $productId);

            if (($categoryId) && (Mage::getStoreConfig('catalog/seo/product_use_categories', $storeId))) {
                if (!Mage::getStoreConfig('alternatelang/config/canonical', $storeId)) {
                    $idPath = sprintf('%s/%d', $idPath, $categoryId);
                }
            }

            $coreUrl->setStoreId($storeId);
            $coreUrl->loadByIdPath($idPath);
            return $coreUrl->getRequestPath();
        } else {
            return false;
        }
    }

    /**
     * @param $productId
     * @param $storeId
     *
     * @return bool
     */
    public function checkProductVisibility($productId, $storeId)
    {
        $_productshop = Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);
        if (($_productshop->getStatus() != 1) || ($_productshop->getVisibility() == 1)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $categoryId
     * @param $storeId
     *
     * @return bool
     */
    public function getCoreCategoryUrl($categoryId, $storeId)
    {
        if ($this->checkCategogyVisibility($categoryId, $storeId)) {
            $coreUrl = Mage::getModel('core/url_rewrite');
            $idPath = sprintf('category/%d', $categoryId);
            $coreUrl->setStoreId($storeId);
            $coreUrl->loadByIdPath($idPath);
            return $coreUrl->getRequestPath();
        } else {
            return false;
        }
    }

    /**
     * @param $categoryId
     * @param $storeId
     *
     * @return bool
     */
    public function checkCategogyVisibility($categoryId, $storeId)
    {
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
        $active = Mage::getModel('catalog/category')->load($categoryId)->getIsActive();
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        return $active;
    }

}