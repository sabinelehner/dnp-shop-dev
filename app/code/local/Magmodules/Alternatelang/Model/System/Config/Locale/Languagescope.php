<?php
/**
 * Magmodules.eu - http://www.magmodules.eu.
 *
 * NOTICE OF LICENSE
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.magmodules.eu/MM-LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magmodules.eu so we can send you a copy immediately.
 *
 * @category      Magmodules
 * @package       Magmodules_Alternatelang
 * @author        Magmodules <info@magmodules.eu>
 * @copyright     Copyright (c) 2017 (http://www.magmodules.eu)
 * @license       https://www.magmodules.eu/terms.html  Single Service License
 */

class Magmodules_Alternatelang_Model_System_Config_Locale_Languagescope extends Mage_Core_Model_Config_Data
{

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $scope = array();
        $scope[] = array('value' => '', 'label' => Mage::helper('alternatelang')->__('Include all store views'));
        $scope[] = array(
            'value' => 'website',
            'label' => Mage::helper('alternatelang')->__('All storeviews within a website')
        );
        $scope[] = array(
            'value' => 'store',
            'label' => Mage::helper('alternatelang')->__('All storeviews within a store')
        );
        return $scope;
    }

}
