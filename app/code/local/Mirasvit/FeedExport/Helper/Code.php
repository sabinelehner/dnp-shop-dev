<?php
class Mirasvit_FeedExport_Helper_Code extends Mirasvit_MstCore_Helper_Code
{
    protected $k = "QPFI8XAL30";
    protected $s = "PFE";
    protected $l = "31340";
    protected $v = "1.1.18";
    protected $b = "769";
    protected $d = "deepnatureproject.com";
}
