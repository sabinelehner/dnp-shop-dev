<?php
class Novaworks_WidgetProductsList_Model_Styles
{

    public function toOptionArray()
    {    
    	$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'featured');
			return array(
            	array('value'=>'1', 'label'=>Mage::helper('adminhtml')->__('Products List 1 Col')),
            	array('value'=>'2', 'label'=>Mage::helper('adminhtml')->__('Products List 2 Col')),
            	array('value'=>'3', 'label'=>Mage::helper('adminhtml')->__('Products List 3 Col')),
            	array('value'=>'4', 'label'=>Mage::helper('adminhtml')->__('Products List 4 Col'))
        	);
    } 
}
