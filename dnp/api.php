<?php


function generateHash($initKey,$remoteDomain)
{
    $date = gmdate('dmY');
    $hash = "";

    for($i = 0; $i <= 200; $i++) 
        $hash = sha1($hash . $initKey . $remoteDomain . $date);
  
    return $hash;
}



function SendRequest($url,$methodname,$xml,$hash,$parameter="")
{
  $url = rtrim($url,'/');
  $url = $url.'/index.php?module=api&action='.$methodname.'&hash='.$hash.$parameter;


  $xml ='<?xml version="1.0" encoding="UTF-8"?>
        <request>
                <status>
                <function>'.$methodname.'</function>
                </status>
                        <xml>'.$xml.'</xml>
                </request>';
  $data = array('xml' => $xml);//, 'md5sum' => md5($xml));

  // use key 'http' even if you send the request to https://...
  $options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data),
    ),
  );
  $context  = stream_context_create($options);
  $result = file_get_contents($url, false, $context);
  return $result;
}



?>
