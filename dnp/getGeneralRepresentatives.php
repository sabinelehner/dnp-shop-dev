<?php

function generateHash() {
    
	$initKey = 'EXsl4ywgDR5mR3sTGwws9Ikl2ocSKZlvhxoFTv8Au3MaV5UqrEIJORVF5PmFNy';
	$remoteDomain = 'devshop.deepnatureproject.com';
	$date = gmdate('dmY');
	$hash = "";
	
	for ($i = 0; $i <= 200; $i++) {
		$hash = sha1($hash . $initKey . $remoteDomain . $date);
	}
	
	return $hash;
	
}

function sendRequest($hash) {
	
	#$url = 'https://wawi.deepnatureproject.com/index.php?module=api&action='.$methodname.'&hash='.$hash;
	$url = 'https://wawi.deepnatureproject.com/www/index.php';
	$data = array('module' => 'api', 'action' => 'ExportVorlageGet', 'hash' => $hash, 'id' => 2);

	/*
	https://wawi.deepnatureproject.com/www/index.php?module=api&action=ExportVorlageGet&hash=3ea04dccc82b0cb5e6a1b6f002ef15ba1289cd05&id=2
	*/

	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data),
		),
	);
	
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	
	echo "context:<br />";
	var_dump($context);
	
	if ($result === FALSE) {
		/* Handle error */
		echo "error";
	} else {
		echo "result:<br />";
		var_dump($result);
	}
	
	#return $result;
}

$hash = generateHash();

sendRequest($hash);

/*
header('Content-Type: application/xml');
$output = "<response>
	<status>
		<action>AdresseGet</action>
		<message>OK</message>
		<messageCode>1</messageCode>
	</status>
	<xml>
		<addresses>
			<address>
				<id>37</id>
				<name>Max Muster</name>
				<land>DE</land>
				<email>maxi@mustermann.at</email>
			</address>
			<address>
				<id>39</id>
				<name>Susi Superschön</name>
				<land>CH</land>
				<email>susi@test.at</email>
			</address>
		</addresses>
	</xml>
</response>";
print ($output);
*/

?>