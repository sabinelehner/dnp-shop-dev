<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Filter/Input/String.php';
//require_once 'Customweb/OPP/Method/DefaultMethod.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/I18n/Translation.php';



/**
 * @Method(paymentMethods={'Afterpay'})
 */
class Customweb_OPP_Method_AfterpayMethod extends Customweb_OPP_Method_DefaultMethod {

	/**
	 *
	 * @return boolean
	 */
	public function isRiskBasedCapturingSupported(){
		return false;
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $customerPaymentContext, $authorizationMethod){
		return array_merge(
				parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $customerPaymentContext, $authorizationMethod), 
				$this->getPhoneNumberElements($orderContext, $customerPaymentContext),
				$this->getBirthdayElements($orderContext, $customerPaymentContext), 
				$this->getGenderElements($orderContext, $customerPaymentContext));
	}

	public function getAuthorizationParameters(Customweb_OPP_Authorization_OppTransaction $transaction, array $formData){
		$parameters = array();
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		
		$shippingPhoneNumber = $orderContext->getShippingAddress()->getPhoneNumber();
		if (!$orderContext->getBillingAddress()->getPhoneNumber() && (!isset($formData['phone_number']) || empty($formData['phone_number']))) {
			throw new Exception('The phone number needs to be set.');
		}
		if(isset($formData['phone_number']) && !empty($formData['phone_number'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'phone' => $formData['phone_number']
			));
			$parameters['customer.phone'] =  Customweb_Filter_Input_String::_($formData['phone_number'], 25)->filter();
			if (empty($shippingPhoneNumber)) {
				$shippingPhoneNumber = $parameters['customer.phone'];
			}
		}
		elseif(empty($shippingPhoneNumber)) {
			$shippingPhoneNumber = $orderContext->getBillingAddress()->getPhoneNumber();
		}
		$parameters['customParameters[AFTERPAY_ShipReferencePerson_Phonenumber1]'] = $shippingPhoneNumber;
				
		$shippingBirtDay = '';
		if($orderContext->getShippingAddress()->getDateOfBirth() instanceof DateTime){
			$shippingBirtDay = $orderContext->getShippingAddress()->getDateOfBirth()->format('Y-m-d');
		}
		if (!($orderContext->getBillingAddress()->getDateOfBirth() instanceof DateTime) && !$this->isDateOfBirthValid($formData)) {
			throw new Exception('The date of birth needs to be set.');
		}
		if ($this->isDateOfBirthValid($formData)) {
			$dateOfBirth = DateTime::createFromFormat('Y-m-d', 
					$formData['date_of_birth_year'] . '-' . $formData['date_of_birth_month'] . '-' . $formData['date_of_birth_day']);
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'birthDate' => $dateOfBirth 
			));
			$parameters['customer.birthDate'] = $dateOfBirth->format('Y-m-d');
			if(empty($shippingBirtDay)){
				$shippingBirtDay = $parameters['customer.birthDate'];
			}
		}
		elseif(empty($shippingBirtDay)){
			$shippingBirtDay = $orderContext->getBillingAddress()->getDateOfBirth()->format('Y-m-d');
		}		
		$parameters['customParameters[AFTERPAY_ShipReferencePerson_Dateofbirth]'] = $shippingBirtDay.'T00:00:00';
		
		
		
		$shippingGender = '';
		if ($orderContext->getShippingAddress()->getGender() == 'male') {
			$shippingGender = 'M';
		}
		elseif ($orderContext->getShippingAddress()->getGender() == 'female') {
			$shippingGender = 'F';
		}		
		if (!$orderContext->getBillingAddress()->getGender() && (!isset($formData['gender']) || empty($formData['gender']))) {
			throw new Exception('The gender needs to be set.');
		}
		if (isset($formData['gender']) && !empty($formData['gender'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'gender' => $formData['gender'] 
			));
			if ($formData['gender'] == 'male') {
				$parameters['customer.sex'] = 'M';
			}
			elseif ($formData['gender'] == 'female') {
				$parameters['customer.sex'] = 'F';
			}
			if(empty($shippingGender)){
				$shippingGender = $parameters['customer.sex'];
			}
		}
		elseif(empty($shippingGender)){
			if ($orderContext->getBillingAddress()->getGender() == 'male') {
				$shippingGender = 'M';
			}
			elseif ($orderContext->getBillingAddress()->getGender() == 'female') {
				$shippingGender = 'F';
			}
		}
		$parameters['customParameters[AFTERPAY_ShipReferencePerson_Gender]'] = $shippingGender;

		$parameters['customParameters[AFTERPAY_BillReferencePerson_IsoLanguage]'] = $orderContext->getLanguage()->getIso2LetterCode();
		$parameters['customParameters[AFTERPAY_ShipReferencePerson_IsoLanguage]'] = $orderContext->getLanguage()->getIso2LetterCode();
		
		if ($orderContext->getCustomerRegistrationDate() instanceof DateTime) {
			$parameters['customParameters[AFTERPAY_Shopper_ProfileCreated]'] = $orderContext->getCustomerRegistrationDate()->format('Y-m-d\TH:i:s');
		}
		
		$shippingEmail = $orderContext->getShippingAddress()->getEMailAddress();
		if (!empty($shippingEmail)) {
			$parameters['customParameters[AFTERPAY_ShipReferencePerson_Emailaddress]'] = $shippingEmail;
		}
		return array_merge(parent::getAuthorizationParameters($transaction, $formData), $parameters);
	}
	

	public function getCartItemParameters(Customweb_OPP_Authorization_OppTransaction $transaction){
		$parameters = array();
		$i = 0;
		$orderContex = $transaction->getTransactionContext()->getOrderContext();
		foreach ($orderContex->getInvoiceItems() as $item) {
	
			if (round($item->getQuantity()) == 0) {
				continue;
			}
			$name = $item->getName();
			if(empty($name)){
				$name = Customweb_I18n_Translation::__('No Name Provided');
			}
	
			$parameters['cart.items[' . $i . '].name']				= Customweb_Filter_Input_String::_($name, 255)->filter();
			$parameters['cart.items[' . $i . '].merchantItemId']	= Customweb_Filter_Input_String::_($item->getSku(), 255)->filter();
			$parameters['cart.items[' . $i . '].quantity']			= round($item->getQuantity());
			$parameters['cart.items[' . $i . '].type']				= '3';
			
			if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
				$price = -1 * $item->getAmountIncludingTax() / round($item->getQuantity());
			} else {
				$price = $item->getAmountIncludingTax() / round($item->getQuantity());
			}
			$parameters['cart.items[' . $i . '].price']				= Customweb_Util_Currency::formatAmount($price, $orderContex->getCurrencyCode(), '', '');
			$parameters['cart.items[' . $i . '].currency']			= $orderContex->getCurrencyCode();
			$parameters['cart.items[' . $i . '].tax']				= number_format($item->getTaxRate(), 1);
			$parameters['cart.items[' . $i . '].description']				= Customweb_Filter_Input_String::_($name, 255)->filter();
			$i++;
		}
		return $parameters;
	}

}