<?php
$running = false;
$pid = "";

echo "<pre>";

exec("ps -x", $output);

#print_r($output);

if (is_array($output)) {
	foreach ($output as $string) {
		$pos = strpos($string, "/usr/bin/ruby18 /usr/bin/sass --watch skin/frontend/dnp/default/scss:skin/frontend/dnp/default/scss");
		
		if ($pos !== false) {
			$running = true;
			$pid = trim(substr($string, 0, strpos($string, '?')));
		}
	}
}

if (isset($_POST["pid"])) {
	#echo "post-pid: " . $_POST["pid"];
	exec("kill " . $_POST["pid"]);
	$running = false;
}

if ($running) {
	echo "process is already running ...<br /><br />";
	echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">';
	echo '<input type="hidden" name="pid" value="' . $pid . '">';
	echo '<button type="submit">restart process</button>';
	echo '</form>';
} else {
	passthru("sass --watch skin/frontend/dnp/default/scss:skin/frontend/dnp/default/scss");
}

echo "</pre>";
?>