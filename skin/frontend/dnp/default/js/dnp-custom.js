
jQuery(document).ready(function() {
								
	Validation.add('validate-company', 'Please enter a company name, not your gender or a number!', function(value) {
		var yearPattern = /^[0-9]+$/;
		if ((value.toLowerCase() == 'frau') || (value.toLowerCase() == 'mann') || (yearPattern.test(value))) {
			return false;
		} else {
			return true;
		}
	})
							   
	jQuery('#medihemp-location').on('click', function(){
		window.open('https://goo.gl/maps/72FK2WMtL292');
	});

	/*								
	var loginForm;
	
	jQuery('.dnp-loginsubmit').on('click', function(e) {
		e.preventDefault();
		
		if (loginForm)
			loginForm.validator.reset();
		
		jQuery('#pass').addClass('required-entry');
		loginForm = new VarienForm('login-form', true); 
		
		jQuery('#login-form').submit();
	})
	
	jQuery('.dnp-initsubmit').on('click', function(e) {
		e.preventDefault();
		
		if (loginForm)
			loginForm.validator.reset();
		
		jQuery('#pass').removeClass('required-entry');
		loginForm = new VarienForm('login-form', true); 
		
		jQuery('#login-form').submit();
	})
	*/
	
	/* --------------------------------------------------
		Hero Headline Animation
	-------------------------------------------------- */
	
	function initAnimation() {	
		new WOW().init();
	}
	
	if (jQuery('.landing-hero').length) {
		initAnimation();
	}
								
	jQuery('#mainnav').affix({
		offset: {
			top: 60
		}
	})
	
	del_confirmed = false;
	
	jQuery('.td-remove a').on('click', function(e){
		if (del_confirmed === true) {
			del_confirmed = false;
			
			var href = jQuery(this).attr('href');
			window.location.href = href;
			
			return;
		}
		
		e.preventDefault();
		
		bootbox.confirm({
			message: Translator.translate('Are you sure you would like to remove this item from the shopping cart?'),
			buttons: {
				confirm: {
					label: Translator.translate('OK'),
					className: 'btn-success'
				},
				cancel: {
					label: Translator.translate('Cancel'),
					className: 'btn-cancel'
				}
			},
			callback: function (result) {
				if (result) {
					del_confirmed = true;
					//jQuery(this).trigger('click');
					jQuery(e.currentTarget).trigger('click');
				} else {
					del_confirmed = false;
				}
			}			
		});
	});
	
	jQuery('.btn-video').on('click', function () {
		var theModal = jQuery(this).data('target'),
		videoSRC = jQuery(this).attr('data-video'),
		videoSRCauto = videoSRC + '?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1';
		jQuery(theModal + ' iframe').attr('src', videoSRCauto);
		jQuery(theModal + ' button.close').click(function () {
			jQuery(theModal + ' iframe').attr('src', videoSRC);
		});
	});
	
	jQuery('#videoModal .close').on('click', function() {
		jQuery('#videoModal iframe').attr('src', jQuery('#videoModal iframe').attr('src'));
	});
	
	jQuery('.btn-cloud').on('click', function () {
		var theModal = jQuery(this).data('target'),
		cloudSRC = jQuery(this).attr('href');
		jQuery(theModal + ' iframe').attr('src', cloudSRC);
		jQuery(theModal + ' button.close').click(function () {
			jQuery(theModal + ' iframe').attr('src', cloudSRC);
		});
	});
							
	// ---------------------- GROUP REGISTRATION ---------------------- //
	
	/*
	if (jQuery('.group_registration_form').length) {
		
		jQuery('button[title=submit]').attr('disabled', 'disabled');

		jQuery('.group_registration_form').each(function(idx) {
			var total = jQuery(this).find('.fieldset').length;
			
			jQuery(this).find('.fieldset').each(function(index) {
				if (index !== total - 1) {
					jQuery(this).find('ul.form-list').append('<li class="buttons"><button type="button" class="button next-step" title="weiter" disabled="disabled" data-toggle="collapse" data-parent="#accordion" data-target="#collapse' + (index + 2) + '"><span><span>' + Translator.translate('Next step') + '</span></span></button></li>');
				}
			});
		});
		
		jQuery('a[data-toggle="collapse"]').on('click', function(e){
			if (jQuery(this).hasClass('blocked'))
				e.stopPropagation();
		});
		
		//jQuery('#accordion').collapse({ parent: true, toggle: true });
		
		//jQuery('.webforms-').accordion({
			//active: false,
			//collapsible: true,
			//header: 'h2',
			//heightStyle: 'content'
		//});
		
		jQuery('.next-step').on('click', function(e) {
			e.preventDefault();
			if (jQuery('.webforms-fields-general-agency-country').find('select').val() != "") {
				jQuery(this).closest('.panel-default').find('.panel-heading').find('a').removeClass('blocked');
				jQuery(this).closest('.panel-collapse').collapse('hide');
				//jQuery(this).closest('.webforms-').accordion('option', 'active', (jQuery(this).closest('.webforms-').accordion('option','active') + 1));
			} else {
				jQuery.modal('<div class="modal-content">' + Translator.translate('Please choose your country!') + '</div>', {
					overlayClose: true
				});
			}
		});
		
	}
	*/
	
	if (jQuery('.group_registration_form').length) {

		jQuery('.group_registration_form').each(function(idx) {
			var total = jQuery(this).find('.fieldset').length;
			
			jQuery(this).find('.fieldset').each(function(index) {
				if (index !== total - 1) {
					jQuery(this).find('ul.form-list').append('<li class="buttons"><button type="button" class="button next-step" title="weiter" data-toggle="collapse" data-parent="#accordion" data-target="#collapse' + (index + 1) + '"><span><span>' + Translator.translate('Next step') + '</span></span></button></li>');
				}
			});
		});
		
		jQuery('.panel-collapse:first').addClass('collapse in');
		
		jQuery('#accordion .collapse').on('show.bs.collapse', function(e) {
			var actives = jQuery('#accordion').find('.in, .collapsing');
			actives.each( function (index, element) {
				jQuery(element).collapse('hide');
			})
		})
		
		/*
		jQuery('.next-step').on('click', function(e) {
			e.preventDefault();
			jQuery(this).closest('.panel-collapse').collapse('hide');
		});
		*/
		
		/*
		jQuery('a[data-toggle="collapse"]').on('click', function(e){
			if (jQuery(this).hasClass('blocked'))
				e.stopPropagation();
		});
		*/
		
		//jQuery('#accordion').collapse({ parent: true, toggle: true });
	}
		
	if (jQuery('#consumer').length) {
		jQuery('label[for=taxvat3]').removeClass('required');
		jQuery('#taxvat3').removeClass('required-entry');
		jQuery('label[for=taxvat3]').parent().hide();
	}
	
	/*
	jQuery('.group_registration_header input[type=radio]').on('click', function() {
		var formID = jQuery(this).attr('id').substring(jQuery(this).attr('id').indexOf('show-') + 5);
		
		jQuery('.webforms-').accordion('option', 'active', 0);
		
		if (jQuery('#' + formID).length) {
			jQuery('.group_registration_form').hide();
			jQuery('#' + formID).show();
		}
	})
	*/
	
	jQuery('.webforms-fields-general-agency-country select').on('change', function() {
		var curCountry = jQuery(this).val();
		var curForm = jQuery(this).closest('.group_registration_form');
		
		jQuery('#backgroundPopup').fadeIn('fast');
		
		if (curCountry) {
			
			jQuery.ajax({
				type: 'POST',
				url: '/dnp/exportvorlage/get_exportvorlage.php',
				dataType: 'xml',
				success: function(xml) {
					jQuery('button[title=submit]').removeAttr('disabled');
					jQuery('.next-step').removeAttr('disabled');
					jQuery('.agency-country-info').remove();
					
					if (jQuery(xml).find('items').length == 0) {
						jQuery.modal(Translator.translate('Unfortunataly an error occured (Errorcode 11).<br />Please contact us at <a href=\"mailto:office@medihemp.at\" target=\"_blank\">office@medihemp.at</a> for further information.'), {overlayClose: true});
					} else {					
						jQuery(xml).find('item').each(function(){
							if (jQuery(this).find('land').text() == curCountry) {
								jQuery('<div class="agency-country-info">' + Translator.translate('There already exists a general representative for your country.<br />Please contact us at <a href=\"mailto:office@medihemp.at\" target=\"_blank\">office@medihemp.at</a> for further information or contact your representative directly at') + ' <a href="mailto:' + jQuery(this).find('email').text() + '" target="_blank">' + jQuery(this).find('email').text() + '</a>.</div>').insertAfter('.webforms-fields-general-agency-country');
								jQuery('button[title=submit]').attr('disabled', 'disabled');
								curForm.find('button.next-step').attr('disabled', 'disabled');
								return false;
							}
						});
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					//alert(JSON.stringify(xhr.status) + ' - ' + thrownError);
					jQuery.modal(Translator.translate('Unfortunataly an error occured (Errorcode 12 - ' + JSON.stringify(xhr.status) + ' - ' + thrownError + ').<br />Please contact us at <a href=\"mailto:office@medihemp.at\" target=\"_blank\">office@medihemp.at</a> for further information.'), {overlayClose: true});
					return false;
				},
				complete:function (jqXHR, textStatus){
					jQuery('#backgroundPopup').fadeOut('fast');
				}
			});
			
		}
	})
	
	// ---------------------------------------------------------------- //
	
							
	// ---------------------- CUSTOM PRODUCT BUNDLE ---------------------- //
	
	var qty = 0;
							
	checkBundleQuantity = function(set_target_qty) {
		
		var cur_qty = 0;
		
		jQuery('.product-options input.input-text.qty').each(function() {			
			cur_qty += parseInt(jQuery(this).val());			
		});
			
		if (set_target_qty) {
			qty = cur_qty;
		}
		
		/*
		alert('qty: ' + qty);
		alert('cur_qty: ' + cur_qty);
		alert(cur_qty < qty);
		alert(cur_qty % qty != 0);
		*/
			
		if ((cur_qty < qty) || (cur_qty % qty != 0)) {
			jQuery('.product-options-bottom button.btn-cart').attr('disabled', 'disabled');
			jQuery.modal('<div class="modal-content">Die Gesamtmenge muss ein Vielfaches von ' + qty + ' betragen!</div>', {overlayClose: true});
		} else {
			jQuery('.product-options-bottom button.btn-cart').removeAttr('disabled');
		}
		
	}
							
	if (jQuery('.product-options input.input-text.qty').length != 0) {
		checkBundleQuantity(true);
	}
							
	jQuery('.product-options input.input-text.qty').on('change', function() {
		checkBundleQuantity(false);
	})			

	// ------------------------------------------------------------------- //
	
  // ---------------------- OPEN CLOSE ---------------------- //

	jQuery('#product_tabs_productfaqs_contents dt').on('click', function() {
    if(jQuery(this).hasClass('open')){
      jQuery(this).next().slideUp();
      jQuery(this).removeClass('open');
    }
    else{
      jQuery(this).next().slideDown();
      jQuery(this).addClass('open');
    }
	});
 
  
	// ------------------------------------------------------------------- //

  // ---------------------- STARRATING ---------------------- //

	jQuery('#customer-reviews #product-review-table .radio ~ label').on('click', function() {
    jQuery(this).parent().addClass('isChecked');
    jQuery(this).parent().prevAll().addClass('isChecked');
    jQuery(this).parent().nextAll().removeClass('isChecked');
	});
 
  
	// ------------------------------------------------------------------- //
	
	// ---------------------- QUICK SEARCH ---------------------- //
		
	jQuery('#header-search #show-search').on('click', function() {
		if (!jQuery('#header-search').hasClass('expanded')) {
			jQuery('#header-search').addClass('expanded');
			jQuery('input#search').focus();
		} else {
			jQuery('#header-search').removeClass('expanded');
		}
	});
	
	// ---------------------------------------------------------- //	
	
	// ---------------------- NAVIGATION---------------------- //
	
  // unbind the links from level1 navigation
    jQuery('#mainnav .parent > a.level-top').on('click', function(event){
            event.preventDefault();
            var submenu = jQuery(this).next('ul');
            if(submenu.is(':visible')){
	           submenu.slideUp();
	           jQuery(this).parent('li').removeClass('dnp-active')
	        }
            else {
              jQuery('#mainnav ul.level0:visible, .arw-toggle-content:visible, .top-link-benz:visible').slideUp();
              submenu.slideDown();
	          jQuery(this).parent('li').addClass('dnp-active')
            }
    });
	// Close Subnavigation when opening other Navigation Content	
	 jQuery('.arw-toggle-control, .btn-headertop').on('click', function(event){
		   jQuery('#mainnav ul.level0:visible').slideUp();
           jQuery('#nav > li').removeClass('dnp-active');
	 });
	 
	 
	// ---------------------------------------------------------- //	
	
	// ---------------------- NAVIGATION ---------------------- //
	
	jQuery(".lightbox").fancybox();
	
	// ---------------------------------------------------------- //	
			
});